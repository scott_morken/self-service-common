@if (!isset($renderer))
    @inject('renderer', 'Smorken\SSCommon\Contracts\Renderers\Render')
@endif
@if ($messages && count($messages))
    @foreach($messages as $message)
        <{{ $message->container }} class="{{ $message->classes }}">
            <?php echo $renderer->render($message->message); ?>
        </{{ $message->container }}>
    @endforeach
@endif
