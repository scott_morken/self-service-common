<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 11:08 AM
 */

namespace Tests\Smorken\SSCommon\Unit\Models\VO;

use Smorken\SSCommon\Models\VO\ModuleOutput;

class ModuleOutputModelVOTest extends \PHPUnit\Framework\TestCase
{

    public function testOptionsFromOptionsArray()
    {
        $data = [
            'options' => [
                'foo' => 'bar',
            ],
        ];
        $sut = new ModuleOutput($data);
        $this->assertEquals('bar', $sut->getOption('foo'));
    }

    public function testOptionsDirectSet()
    {
        $data = [
            'foo' => 'bar',
        ];
        $sut = new ModuleOutput($data);
        $this->assertEquals('bar', $sut->getOption('foo'));
    }
}
