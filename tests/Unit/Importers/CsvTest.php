<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:24 AM
 */

namespace Tests\Smorken\SSCommon\Unit\Importers;

use Mockery as m;

class CsvTest extends \PHPUnit\Framework\TestCase
{

    public function setUp(): void
    {
        if (!file_exists(__DIR__.'/test.csv')) {
            $this->makeTestFile();
        }
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testImportAndUnlink()
    {
        list($sut, $o, $l) = $this->getSut();
        $o->shouldReceive('getExtIdToIdMap')->once()->andReturn(['PCC01' => '1']);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG1',
                'descr' => 'Building 1',
                'latitude' => '30.1111',
                'longitude' => '15.1111',
                'org_id' => '1',
            ]
        )->andReturn(true);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG2',
                'descr' => 'Building 2',
                'latitude' => '30.2222',
                'longitude' => '15.2222',
                'org_id' => '1',
            ]
        )->andReturn(true);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG3',
                'descr' => 'Building 3',
                'latitude' => '30.3333',
                'longitude' => '15.3333',
                'org_id' => '1',
            ]
        )->andReturn(true);
        $count = $sut->import(__DIR__.'/test.csv');
        $this->assertEquals(3, $count);
        $this->assertFalse(file_exists(__DIR__.'/test.csv'));
    }

    public function testImportWithError()
    {
        list($sut, $o, $l) = $this->getSut();
        $o->shouldReceive('getExtIdToIdMap')->once()->andReturn(['PCC01' => '1']);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG1',
                'descr' => 'Building 1',
                'latitude' => '30.1111',
                'longitude' => '15.1111',
                'org_id' => '1',
            ]
        )->andReturn(true);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG2',
                'descr' => 'Building 2',
                'latitude' => '30.2222',
                'longitude' => '15.2222',
                'org_id' => '1',
            ]
        )->andReturn(true);
        $l->shouldReceive('import')->with(
            [
                'ext_location_id' => 'BLDG3',
                'descr' => 'Building 3',
                'latitude' => '30.3333',
                'longitude' => '15.3333',
                'org_id' => '1',
            ]
        )->andReturn(false);
        $count = $sut->import(__DIR__.'/test.csv');
        $this->assertEquals(2, $count);
        $this->assertFalse(file_exists(__DIR__.'/test.csv'));
        $this->assertTrue($sut->hasErrors());
        $this->assertEquals(
            'PCC01,BLDG3,Building 3,30.3333,15.3333',
            $sut->getErrors()[0]
        );
    }

    protected function getSut($delimiter = ',', $enclosure = '"', $escape = "\\")
    {
        $o = m::mock(\Smorken\SSCommon\Contracts\Storage\Org::class);
        $l = m::mock(\Smorken\SSCommon\Contracts\Storage\Location::class);
        $sut = new \Smorken\SSCommon\Importers\Csv($o, $l, $delimiter, $enclosure, $escape);
        return [$sut, $o, $l];
    }

    protected function makeTestFile()
    {
        $data = '"PCC01","BLDG1","Building 1",30.1111,15.1111
"PCC01","BLDG2","Building 2",30.2222,15.2222
"PCC01","BLDG3","Building 3",30.3333,15.3333';
        file_put_contents(__DIR__.'/test.csv', $data);
    }
}
