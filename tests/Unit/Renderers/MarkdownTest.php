<?php

namespace Tests\Smorken\SSCommon\Unit\Renderers;

use League\CommonMark\Converter;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;
use Smorken\Sanitizer\Actors\RdsCds;
use Smorken\Sanitizer\Actors\Standard;
use Smorken\Sanitizer\Actors\Xss;

class MarkdownTest extends \PHPUnit\Framework\TestCase
{

    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        $environment = Environment::createCommonMarkEnvironment();
        $environment->mergeConfig(['safe' => true]);
        $docParser = new DocParser($environment);
        $htmlRenderer = new HtmlRenderer($environment);

        $converter = new Converter($docParser, $htmlRenderer);
        $opts = [
            'default' => 'standard',
            'sanitizers' => [
                'standard' => Standard::class,
                'sis' => RdsCds::class,
                'xss' => Xss::class,
            ],
            'purifier_options' => [
                'Core.Encoding' => 'UTF-8',
                'AutoFormat.AutoParagraph' => true,
                'AutoFormat.RemoveEmpty' => true,
            ],
        ];
        $sanitize = new \Smorken\Sanitizer\Sanitize($opts);
        $this->sut = new \Smorken\SSCommon\Renderers\Markdown($converter, $sanitize);
    }

    public function testRenderSimple()
    {
        $e = '<p>foo</p>'.PHP_EOL;
        $this->assertEquals($e, $this->sut->render('foo'));
    }

    public function testRenderWithMarkdown()
    {
        $e = '<h1>Foo</h1>'.PHP_EOL;
        $this->assertEquals($e, $this->sut->render('# Foo'));
    }

    public function testRenderWithScript()
    {
        $e = '<h1>Fooalert(1);</h1>'.PHP_EOL;
        $this->assertEquals($e, $this->sut->render('# Foo<script>alert(1);</script>'));
    }
}
