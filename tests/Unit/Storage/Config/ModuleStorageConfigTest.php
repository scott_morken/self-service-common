<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 10:02 AM
 */
namespace Tests\Smorken\SSCommon\Unit\Storage\Config;

include_once __DIR__ . '/helpers.php';

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Mockery as m;

class ModuleStorageConfigTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetModules()
    {
        [$sut, $router] = $this->createSut();
        $modules = $sut->getModules();
        $this->assertCount(4, $modules);
        $this->assertTrue($modules->has('StudentClassModule'));
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Module::class, $modules->first());
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Route::class, $modules->first()->routes->first());
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\ModuleOutput::class, $modules->first()->outputs->first());
    }

    public function testGetModule()
    {
        [$sut, $router] = $this->createSut();
        $module = $sut->getModule('StudentClassModule');
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Module::class, $module);
        $this->assertCount(3, $module->routes);
        $this->assertCount(2, $module->outputs);
    }

    public function testGetRoutesFrom()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Route::class, $routes->first());
        $this->assertEquals('foo_action', $routes->first()->action);
        $this->assertEquals('/bar', $routes->first()->path);
    }

    public function testGetRoutesFromRegistersRoutes()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'controller' => 'baz',
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $registered = $sut->getRegisteredRoutes();
        $expected = [
            'middleware' => ['web', 'auth'],
            'uses' => 'baz@foo_action',
            'controller' => 'baz@foo_action',
        ];
        $this->assertCount(1, $registered);
        $route = $registered->get('CORE')->get('baz@foo_action');
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals('bar', $route->uri);
        $this->assertEquals($expected, $route->action);
    }

    public function testGetRoutesFromWithBaseRegistersRoutes()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'base' => 'foo',
            'controller' => 'baz',
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $registered = $sut->getRegisteredRoutes();
        $expected = [
            'middleware' => ['web', 'auth'],
            'uses' => 'baz@foo_action',
            'controller' => 'baz@foo_action',
        ];
        $this->assertCount(1, $registered);
        $route = $registered->get('CORE')->get('baz@foo_action');
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals('foo/bar', $route->uri);
        $this->assertEquals($expected, $route->action);
    }

    protected function createSut($override = [])
    {
        $router = new Router(new Dispatcher(), new Container());
        $om = new \Smorken\SSCommon\Models\VO\ModuleOutput();
        $rm = new \Smorken\SSCommon\Models\VO\Route();
        $m = new \Smorken\SSCommon\Models\VO\Module();
        $sut = new \Smorken\SSCommon\Storage\Config\Module($m, $rm, $om, $router, $this->getConfig($override));
        return [$sut, $router];
    }

    protected function getConfig($override = [])
    {
        $config = [
            'active' => [
                'StudentClassModule' => [
                    'name'         => 'My Classes',
                    'controller'   => 'App\Http\Controllers\Core\Module\StudentClassModule',
                    'action'       => 'App\Http\Controllers\Core\Module\StudentClassModule@index',
                    'base'         => 'student-class',
                    'icon'         => 'glyphicon glyphicon-calendar',
                    'display_both' => true,
                    'routes'       => [
                        'index'          => '/{term_id?}',
                        'email'          => '/email/{term_id}',
                        'googleCalendar' => '/google-calendar/{term_id}',
                    ],
                    'output'       => [
                        'email'          => [
                            'view'    => 'core.module.stuclass.email',
                            'name'    => 'Email',
                            'action'  => 'App\Http\Controllers\Core\Module\StudentClassModule@email',
                            'icon'    => 'glyphicon glyphicon-envelope',
                            'params'  => [
                                'term_id' => 'term_id',
                            ],
                            'options' => [ //3.1+ style
                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                                'subject' => 'Your PC class schedule',
                                'ical'    => false,
                            ],
                        ],
                        'googlecalendar' => [
                            'show_nav' => false,
                            'name'     => 'Send to my Google Calendar',
                            'action'   => 'App\Http\Controllers\Core\Module\StudentClassModule@googleCalendar',
                            'icon'     => 'glyphicon glyphicon-calendar',
                            'params'   => [
                                'term_id' => 'term_id',
                            ],
                            'helper'   => 'App\Helper\Google\CalendarHelper',//old style
                        ],
                    ],
                ],
                'BookModule'         => [
                    'name'         => 'My Books',
                    'controller'   => 'App\Http\Controllers\Core\Module\BookModule',
                    'action'       => 'App\Http\Controllers\Core\Module\BookModule@index',
                    'base'         => 'books',
                    'icon'         => 'glyphicon glyphicon-book',
                    'display_both' => true,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        'email' => '/email/{term_id}',
                    ],
                    'output'       => [
                        'email' => [
                            'view'    => 'core.module.book.email',
                            'name'    => 'Email',
                            'action'  => 'App\Http\Controllers\Core\Module\BookModule@email',
                            'icon'    => 'glyphicon glyphicon-envelope',
                            'params'  => [
                                'term_id' => 'term_id',
                            ],
                            'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                            'subject' => 'Your PC books',
                        ],
                    ],
                ],
                'GradeModule'        => [
                    'name'         => 'My Grades',
                    'controller'   => 'App\Http\Controllers\Core\Module\GradeModule',
                    'action'       => 'App\Http\Controllers\Core\Module\GradeModule@index',
                    'base'         => 'grade',
                    'icon'         => 'glyphicon glyphicon-education',
                    'display_both' => true,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        'email' => '/email/{term_id}',
                    ],
                    'output'       => [
                        'email' => [
                            'view'    => 'core.module.grade.email',
                            'name'    => 'Email',
                            'action'  => 'App\Http\Controllers\Core\Module\GradeModule@email',
                            'icon'    => 'glyphicon glyphicon-envelope',
                            'params'  => [
                                'term_id' => 'term_id',
                            ],
                            'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                            'subject' => 'Your PC grades',
                        ],
                    ],
                ],
                'CardBalanceModule'  => [
                    'name'         => 'My Print Balance',
                    'controller'   => 'App\Http\Controllers\Core\Module\CardBalanceModule',
                    'action'       => 'App\Http\Controllers\Core\Module\CardBalanceModule@index',
                    'icon'         => 'glyphicon glyphicon-usd',
                    'display_both' => true,
                    'base'         => 'balance',
                    'routes'       => [
                        'index' => '/',
                    ],
                ],
            ],
        ];
        $config['active'] = array_merge($config['active'], $override);
        return $config;
    }
}
