<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 10:02 AM
 */

namespace Tests\Smorken\SSCommon\Unit\Storage\Config;

include_once __DIR__.'/helpers.php';

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Mockery as m;

class AppletStorageConfigTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetApplet()
    {
        [$sut, $router] = $this->createSut();
        $applet = $sut->getApplet('holds');
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Applet::class, $applet);
        $this->assertCount(0, $applet->routes);
    }

    public function testGetApplets()
    {
        [$sut, $router] = $this->createSut();
        $applets = $sut->getApplets();
        $this->assertCount(3, $applets);
        $this->assertCount(1, $applets->get('links')->routes);
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Applet::class, $applets->first());
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Route::class,
            $applets->get('links')->routes->first());
    }

    public function testGetRoutesFrom()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $this->assertInstanceOf(\Smorken\SSCommon\Contracts\Models\Route::class, $routes->first());
        $this->assertEquals('foo_action', $routes->first()->action);
        $this->assertEquals('/bar', $routes->first()->path);
    }

    public function testGetRoutesFromRegistersRoutes()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'controller' => 'baz',
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $registered = $sut->getRegisteredRoutes();
        $expected = [
            'middleware' => ['web', 'auth'],
            'uses' => 'baz@foo_action',
            'controller' => 'baz@foo_action',
        ];
        $this->assertCount(1, $registered);
        $route = $registered->get('CORE')->get('baz@foo_action');
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals('bar', $route->uri);
        $this->assertEquals($expected, $route->action);
    }

    public function testGetRoutesFromWithBaseRegistersRoutes()
    {
        [$sut, $router] = $this->createSut();
        $config = [
            'base' => 'foo',
            'controller' => 'baz',
            'routes' => [
                'foo_action' => '/bar',
            ],
        ];
        $routes = $sut->getRoutesFrom($config, 'CORE');
        $registered = $sut->getRegisteredRoutes();
        $expected = [
            'middleware' => ['web', 'auth'],
            'uses' => 'baz@foo_action',
            'controller' => 'baz@foo_action',
        ];
        $this->assertCount(1, $registered);
        $route = $registered->get('CORE')->get('baz@foo_action');
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals('foo/bar', $route->uri);
        $this->assertEquals($expected, $route->action);
    }

    protected function createSut($override = [])
    {
        $router = new Router(new Dispatcher(), new Container());
        $rm = new \Smorken\SSCommon\Models\VO\Route();
        $m = new \Smorken\SSCommon\Models\VO\Applet();
        $sut = new \Smorken\SSCommon\Storage\Config\Applet($m, $rm, $router, $this->getConfig($override));
        return [$sut, $router];
    }

    protected function getConfig($override = [])
    {
        $config = [
            'active' => [
                'holds' => [
                    'controller' => 'App\Http\Controllers\Core\Applet\ServInd\ServIndApplet',
                    'handler' => 'App\Applet\ServInd\ServIndApplet',
                    'provider' => 'App\Contracts\Storage\Sis\ServiceIndicator',
                    'applet_view' => 'core.applet.servind.applet',
                ],
                'links' => [
                    'controller' => 'App\Http\Controllers\Core\Applet\Link\LinkApplet',
                    'handler' => 'App\Applet\Link\LinkApplet',
                    'provider' => 'Smorken\SSCommon\Contracts\Storage\Applet\Link',
                    'applet_view' => 'core.applet.link.applet',
                    'base' => 'link',
                    'routes' => [
                        'index' => '/',
                    ],
                ],
                'messages' => [
                    'controller' => 'App\Http\Controllers\Core\Applet\Message\MessageApplet',
                    'handler' => 'App\Applet\Message\MessageApplet',
                    'provider' => 'Smorken\SSCommon\Contracts\Storage\Applet\Message',
                    'applet_view' => 'core.applet.message.applet',
                    'base' => '',
                ],
            ],
        ];
        $config['active'] = array_merge($config['active'], $override);
        return $config;
    }
}
