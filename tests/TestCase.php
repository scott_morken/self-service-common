<?php


namespace Tests\Smorken\SSCommon;


use Tests\Smorken\SSCommon\Traits\CreatesApplication;

class TestCase extends \Illuminate\Foundation\Testing\TestCase
{

    use CreatesApplication;
}
