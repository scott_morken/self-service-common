<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 8:01 AM
 */

namespace Tests\Smorken\SSCommon\End2end\Models;

use Illuminate\Support\Facades\Facade;
use Mockery as m;
use Tests\Smorken\SSCommon\End2end\Setup\CreateTablesForTest;
use Tests\Smorken\SSCommon\End2end\Setup\Resolver;
use Tests\Smorken\SSCommon\TestCase;

class LockE2ETest extends TestCase
{

    protected $resolver;

    public function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        $this->setupDB();
    }

    public function tearDown(): void
    {
        m::close();
        $this->resolver = null;
        Resolver::reset();
    }

    public function testIncrementFailures()
    {
        $l = $this->createModel(['attempts' => 4], true);
        $l->incrementFailures();
        $this->assertEquals(5, $l->attempts);
    }

    public function testLockedWithTooManyAttemptsAndInsideTime()
    {
        $l = $this->createModel(['attempts' => 6]);
        $this->assertTrue($l->isLocked(5, 60 * 3));
    }

    public function testNotLockedWithTooFewAttempts()
    {
        $l = $this->createModel(['attempts' => 3]);
        $this->assertFalse($l->isLocked(5, 60 * 3));
    }

    public function testNotLockedWithTooManyAttemptsAndOutsideTime()
    {
        $past = time() - 60 * 5;
        $ud = \Carbon\Carbon::createFromTimestamp($past);
        $l = $this->createModel(['attempts' => 6, 'updated_at' => $ud]);
        $this->assertFalse($l->isLocked(5, 60 * 3));
    }

    public function testNotLockedWithTooManyAttemptsAndOutsideTimeDeletesModel()
    {
        $past = time() - 60 * 5;
        $ud = \Carbon\Carbon::createFromTimestamp($past);
        $l = $this->createModel(['attempts' => 6, 'updated_at' => $ud], true);
        $this->assertTrue($l->exists);
        $this->assertFalse($l->isLocked(5, 60 * 3));
        $this->assertFalse($l->exists);
    }

    protected function createModel($attrs = [], $save = false)
    {
        $default = [
            'user_id' => 1, 'attempts' => 1,
        ];
        foreach ($default as $c => $v) {
            if (!isset($attrs[$c])) {
                $attrs [$c] = $v;
            }
        }
        $m = $this->getModel(\Smorken\SSCommon\Models\Eloquent\Lock::class, $attrs);
        if ($save) {
            $m->save();
        }
        return $m;
    }

    protected function getConnectionResolver()
    {
        if (!$this->resolver) {
            $this->resolver = Resolver::get();
        }
        return $this->resolver;
    }

    protected function getModel($model_class, $attrs = [])
    {
        $model = new $model_class();
        forward_static_call([$model_class, 'setConnectionResolver'], $this->getConnectionResolver());
        $model = $model->forceFill($attrs);
        return $model;
    }

    protected function setupDB()
    {
        $db = m::mock('DB');
        $db->shouldReceive('connection')->andReturn(Resolver::connection());
        $this->app['db'] = $db;
        Facade::setFacadeApplication($this->app);
        $m = new CreateTablesForTest();
        $m->up();
    }

    protected function tearDownDB()
    {
        $db = m::mock('DB');
        $db->shouldReceive('connection')->andReturn(Resolver::connection());
        $this->app['db'] = $db;
        Facade::setFacadeApplication($this->app);
        $m = new CreateTablesForTest();
        $m->down();
    }
}
