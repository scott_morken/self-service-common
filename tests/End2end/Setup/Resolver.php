<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:40 AM
 */

namespace Tests\Smorken\SSCommon\End2end\Setup;

use Illuminate\Database\ConnectionResolver;
use Smorken\Ext\Database\SQLiteConnection;

class Resolver
{

    /**
     * @var ConnectionResolver
     */
    protected static $resolver;

    protected static $connections = ['test' => null];

    public static function get($dsn = 'sqlite::memory:', $username = null, $pw = null, $opts = [])
    {
        if (!self::$resolver) {
            self::connection($dsn, $username, $pw, $opts);
            $resolver = new ConnectionResolver(self::$connections);
            $resolver->setDefaultConnection('test');
            self::$resolver = $resolver;
        }
        return self::$resolver;
    }

    public static function connection($dsn = 'sqlite::memory:', $username = null, $pw = null, $opts = [])
    {
        if (!self::$connections['test']) {
            $pdo = new \PDO($dsn, $username, $pw, $opts);
            $conn = new SQLiteConnection($pdo);
            self::$connections['test'] = $conn;
        }
        return self::$connections['test'];
    }

    public static function reset()
    {
        self::$resolver = null;
    }
}
