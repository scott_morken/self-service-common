<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 8:30 AM
 */

namespace Tests\Smorken\SSCommon\End2end\Storage;

use Illuminate\Support\Facades\Facade;
use Mockery as m;
use Tests\Smorken\SSCommon\End2end\Setup\CreateTablesForTest;
use Tests\Smorken\SSCommon\End2end\Setup\Resolver;
use Tests\Smorken\SSCommon\TestCase;

class LockStorageE2ETest extends TestCase
{

    protected $resolver;

    public function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        $this->setupDB();
    }

    public function tearDown(): void
    {
        m::close();
        $this->resolver = null;
        $this->tearDownDB();
        Resolver::reset();
    }

    public function testIncrementFailuresWithExistingModel()
    {
        $sut = $this->getSut();
        $this->createModel(['attempts' => 6], true);
        $this->assertTrue($sut->incrementFailures(1));
        $m = $this->getModel(\Smorken\SSCommon\Models\Eloquent\Lock::class);
        $l = $m->where('user_id', 1)->first();
        $this->assertEquals(7, $l->attempts);
    }

    public function testIncrementFailuresWithNoExistingModel()
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->incrementFailures(1));
        $m = $this->getModel(\Smorken\SSCommon\Models\Eloquent\Lock::class);
        $l = $m->where('user_id', 1)->first();
        $this->assertEquals(1, $l->attempts);
    }

    public function testLockedWithTooManyAttemptsAndInsideTime()
    {
        $sut = $this->getSut();
        $this->createModel(['attempts' => 6], true);
        $this->assertTrue($sut->isLocked(1, 5, 60 * 3));
    }

    public function testNotLockedWithTooFewAttempts()
    {
        $sut = $this->getSut();
        $this->createModel(['attempts' => 3], true);
        $this->assertFalse($sut->isLocked(1, 5, 60 * 3));
    }

    public function testNotLockedWithTooManyAttemptsAndOutsideTime()
    {
        $past = time() - 60 * 5;
        $ud = \Carbon\Carbon::createFromTimestamp($past);
        $this->createModel(['attempts' => 6, 'updated_at' => $ud], true);
        $sut = $this->getSut();
        $this->assertFalse($sut->isLocked(1, 5, 60 * 3));
    }

    protected function createModel($attrs = [], $save = false)
    {
        $default = [
            'user_id' => 1, 'attempts' => 1,
        ];
        foreach ($default as $c => $v) {
            if (!isset($attrs[$c])) {
                $attrs [$c] = $v;
            }
        }
        $m = $this->getModel(\Smorken\SSCommon\Models\Eloquent\Lock::class, $attrs);
        if ($save) {
            $m = $m->save();
        }
        return $m;
    }

    protected function getConnectionResolver()
    {
        if (!$this->resolver) {
            $this->resolver = Resolver::get();
        }
        return $this->resolver;
    }

    protected function getModel($model_class, $attrs = [])
    {
        $model = new $model_class();
        forward_static_call([$model_class, 'setConnectionResolver'], $this->getConnectionResolver());
        $model = $model->forceFill($attrs);
        return $model;
    }

    protected function getSut()
    {
        $m = $this->getModel(\Smorken\SSCommon\Models\Eloquent\Lock::class);
        return new \Smorken\SSCommon\Storage\Eloquent\Lock($m);
    }

    protected function setupDB()
    {
        $db = m::mock('DB');
        $db->shouldReceive('connection')->andReturn(Resolver::connection());
        $this->app['db'] = $db;
        Facade::setFacadeApplication($this->app);
        $m = new CreateTablesForTest();
        $m->up();
    }

    protected function tearDownDB()
    {
        $db = m::mock('DB');
        $db->shouldReceive('connection')->andReturn(Resolver::connection());
        $this->app['db'] = $db;
        Facade::setFacadeApplication($this->app);
        $m = new CreateTablesForTest();
        $m->down();
    }
}
