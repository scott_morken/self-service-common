<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:57 AM
 */
class ModuleSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $this->createModules();
    }

    protected function createModules()
    {
        foreach ($this->getCoreModuleData() as $id => $module) {
            $this->createModule('CORE', $module, true);
        }
        foreach ($this->getKioskModuleData() as $id => $module) {
            $this->createModule('KIOSK', $module, true);
        }
    }

    protected function createModule($type, $module, $active)
    {
        $type = strtoupper($type);
        $routes = isset($module['routes']) ? $module['routes'] : [];
        $output = isset($module['output']) ? $module['output'] : [];
        unset($module['routes'], $module['output']);
        $module['type'] = $type;
        $module['active'] = $active;
        $m = factory(\Smorken\SSCommon\Models\Eloquent\Module::class)->create($module);
        foreach ($routes as $route_name => $route) {
            factory(\Smorken\SSCommon\Models\Eloquent\Route::class)->create(
                [
                    'action'    => $route_name,
                    'path'      => $route,
                    'parent'    => 'MOD',
                    'parent_id' => $m->id,
                ]
            );
        }

        foreach ($output as $c => $out) {
            $out['module_id'] = $m->id;
            $out['code'] = $c;
            factory(\Smorken\SSCommon\Models\Eloquent\ModuleOutput::class)->create($out);
        }
    }

    protected function getCoreModuleData()
    {
        return [
            'StudentClassModule'   =>
                [
                    'code'         => 'StudentClassModule',
                    'name'         => 'My Classes',
                    'controller'   => 'App\Http\Controllers\Core\Module\StudentClassModule',
                    'action'       => 'App\Http\Controllers\Core\Module\StudentClassModule@index',
                    'base'         => 'student-class',
                    'icon'         => 'glyphicon glyphicon-calendar',
                    'display_both' => true,
                    'order'        => 1,
                    'routes'       => [
                        'index'          => '/{term_id?}',
                        'email'          => '/email/{term_id}',
                        'googleCalendar' => '/google-calendar/{term_id}',
                    ],
                    'output'       => [
                        'email'          => [
                            'view'         => 'core.module.stuclass.email',
                            'name'         => 'Email',
                            'action'       => 'App\Http\Controllers\Core\Module\StudentClassModule@email',
                            'icon'         => 'glyphicon glyphicon-envelope',
                            'display_both' => false,
                            'params'       => [
                                'term_id' => 'term_id',
                            ],
                            'options'      => [
                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                                'subject' => 'Your PC class schedule',
                                'ical'    => false,
                            ],
                        ],
                        'googlecalendar' => [
                            'show_nav'     => false,
                            'view'         => '',
                            'name'         => 'Send to my Google Calendar',
                            'action'       => 'App\Http\Controllers\Core\Module\StudentClassModule@googleCalendar',
                            'icon'         => 'glyphicon glyphicon-calendar',
                            'display_both' => true,
                            'params'       => [
                                'term_id' => 'term_id',
                            ],
                            'options'      => [
                                'helper' => 'App\Helper\Google\CalendarHelper',
                            ],
                        ],
                    ],
                ],
            'BookModule'           =>
                [
                    'code'         => 'BookModule',
                    'name'         => 'My Books',
                    'controller'   => 'App\Http\Controllers\Core\Module\BookModule',
                    'action'       => 'App\Http\Controllers\Core\Module\BookModule@index',
                    'base'         => 'books',
                    'icon'         => 'glyphicon glyphicon-book',
                    'display_both' => true,
                    'order'        => 2,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        'email' => '/email/{term_id}',
                    ],
                    'output'       => [
                        'email' => [
                            'view'    => 'core.module.book.email',
                            'name'    => 'Email',
                            'action'  => 'App\Http\Controllers\Core\Module\BookModule@email',
                            'icon'    => 'glyphicon glyphicon-envelope',
                            'params'  => [
                                'term_id' => 'term_id',
                            ],
                            'options' => [
                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                                'subject' => 'Your PC books',
                            ],
                        ],
                    ],
                ],
            'GradeModule'          =>
                [
                    'code'         => 'GradeModule',
                    'name'         => 'My Grades',
                    'controller'   => 'App\Http\Controllers\Core\Module\GradeModule',
                    'action'       => 'App\Http\Controllers\Core\Module\GradeModule@index',
                    'base'         => 'grade',
                    'icon'         => 'glyphicon glyphicon-education',
                    'display_both' => true,
                    'order'        => 3,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        'email' => '/email/{term_id}',
                    ],
                    'output'       => [
                        'email' => [
                            'view'    => 'core.module.grade.email',
                            'name'    => 'Email',
                            'action'  => 'App\Http\Controllers\Core\Module\GradeModule@email',
                            'icon'    => 'glyphicon glyphicon-envelope',
                            'params'  => [
                                'term_id' => 'term_id',
                            ],
                            'options' => [
                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                                'subject' => 'Your PC grades',
                            ],
                        ],
                    ],
                ],
            'FinancialAidModuleIB' =>
                [
                    'code'         => 'FinancialAidModuleIB',
                    'name'         => 'Financial Aid',
                    'controller'   => 'App\Http\Controllers\Core\Module\IB\FinancialAidModule',
                    'action'       => 'App\Http\Controllers\Core\Module\IB\FinancialAidModule@index',
                    'base'         => 'finaid.ib',
                    'icon'         => 'glyphicon glyphicon-gift',
                    'display_both' => true,
                    'order'        => 5,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        //'email' => '/email/{term_id}',
                    ],
                    'output'       => [
//                        'email' => [
//                            'view'    => 'core.module.finaid.email',
//                            'name'    => 'Email',
//                            'action'  => 'App\Http\Controllers\Core\Module\FinancialAidModule@email',
//                            'icon'    => 'glyphicon glyphicon-envelope',
//                            'params'  => [
//                                'year' => 'year',
//                            ],
//                            'options' => [
//                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
//                                'subject' => 'Your Financial Aid',
//                            ],
//                        ],
                    ],
                ],
            'FinancialAidModule'   =>
                [
                    'code'         => 'FinancialAidModule',
                    'name'         => 'Financial Aid',
                    'controller'   => 'App\Http\Controllers\Core\Module\FinancialAidModule',
                    'action'       => 'App\Http\Controllers\Core\Module\FinancialAidModule@index',
                    'base'         => 'finaid',
                    'icon'         => 'glyphicon glyphicon-gift',
                    'display_both' => true,
                    'order'        => 6,
                    'routes'       => [
                        'index' => '/{term_id?}',
                        //'email' => '/email/{term_id}',
                    ],
                    'output'       => [
//                        'email' => [
//                            'view'    => 'core.module.finaid.email',
//                            'name'    => 'Email',
//                            'action'  => 'App\Http\Controllers\Core\Module\FinancialAidModule@email',
//                            'icon'    => 'glyphicon glyphicon-envelope',
//                            'params'  => [
//                                'year' => 'year',
//                            ],
//                            'options' => [
//                                'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
//                                'subject' => 'Your Financial Aid',
//                            ],
//                        ],
                    ],
                ],
//            'CardBalanceModule'  =>
//                [
//                    'name'         => 'My Print Balance',
//                    'controller'   => 'App\Http\Controllers\Core\Module\CardBalanceModule',
//                    'action'       => 'App\Http\Controllers\Core\Module\CardBalanceModule@index',
//                    'icon'         => 'glyphicon glyphicon-usd',
//                    'display_both' => true,
//                    'order'        => 4,
//                    'base'         => 'balance',
//                    'routes'       => [
//                        'index' => '/',
//                    ],
//                ],
        ];
    }

    public function getKioskModuleData()
    {
        return [
            'StudentClassModule' => [
                'code'       => 'StudentClassModule',
                'name'       => 'My Classes',
                'controller' => 'App\Http\Controllers\Kiosk\Module\StudentClassModule',
                'action'     => 'App\Http\Controllers\Kiosk\Module\StudentClassModule@index',
                'base'       => 'student-class',
                'icon'       => '',
                'order'      => 1,
                'routes'     => [
                    'getPrint'       => '/print/{term_id}',
                    'email'          => '/email/{term_id}',
                    'googleCalendar' => '/google-calendar/{term_id}',
                ],
                'output'     => [
                    'email'          => [
                        'view'    => 'core.module.stuclass.email',
                        'name'    => 'Email',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\StudentClassModule@email',
                        'icon'    => 'glyphicon glyphicon-envelope',
                        'params'  => [
                            'term_id' => 'current_term_id',
                        ],
                        'options' => [
                            'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                            'subject' => 'Your PC class schedule',
                            'ical'    => false,
                        ],
                    ],
                    'googlecalendar' => [
                        'show_nav' => false,
                        'name'     => 'My Calendar',
                        'view'     => '',
                        'action'   => 'App\Http\Controllers\Kiosk\Module\StudentClassModule@googleCalendar',
                        'icon'     => 'glyphicon glyphicon-calendar',
                        'params'   => [
                            'term_id' => 'current_term_id',
                        ],
                        'options'  => [
                            'helper' => 'App\Helper\Google\CalendarHelper',
                        ],
                    ],
                    'print'          => [
                        'view'    => 'kiosk.module.stuclass.print',
                        'name'    => 'Print',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\StudentClassModule@getPrint',
                        'params'  => [
                            'term_id' => 'current_term_id',
                        ],
                        'options' => [
                            'attributes' => [
                                'target="_blank"',
                            ],
                        ],
                    ],
                ],
            ],
            'BookModule'         => [
                'code'       => 'BookModule',
                'name'       => 'My Books',
                'controller' => 'App\Http\Controllers\Kiosk\Module\BookModule',
                'action'     => 'App\Http\Controllers\Kiosk\Module\BookModule@index',
                'base'       => 'books',
                'icon'       => '',
                'order'      => 2,
                'routes'     => [
                    'email'    => '/email/{term_id}',
                    'getPrint' => '/print/{term_id}',
                ],
                'output'     => [
                    'email' => [
                        'view'    => 'core.module.book.email',
                        'name'    => 'Email',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\BookModule@email',
                        'icon'    => 'glyphicon glyphicon-envelope',
                        'params'  => [
                            'term_id' => 'current_term_id',
                        ],
                        'options' => [
                            'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                            'subject' => 'Your PC books',
                        ],
                    ],
                    'print' => [
                        'view'    => 'kiosk.module.book.print',
                        'name'    => 'Print',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\BookModule@getPrint',
                        'params'  => [
                            'term_id' => 'current_term_id',
                        ],
                        'options' => [
                            'attributes' => [
                                'target="_blank"',
                            ],
                        ],
                    ],
                ],
            ],
            'GradeModule'        => [
                'code'       => 'GradeModule',
                'name'       => 'My Grades',
                'controller' => 'App\Http\Controllers\Kiosk\Module\GradeModule',
                'action'     => 'App\Http\Controllers\Kiosk\Module\GradeModule@index',
                'icon'       => '',
                'base'       => 'grade',
                'order'      => 3,
                'routes'     => [
                    'email'    => '/email/{term_id}',
                    'getPrint' => '/print/{term_id}',
                ],
                'output'     => [
                    'email' => [
                        'view'    => 'core.module.grade.email',
                        'name'    => 'Email',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\GradeModule@email',
                        'icon'    => 'glyphicon glyphicon-envelope',
                        'params'  => [
                            'term_id' => 'last_term_id',
                        ],
                        'options' => [
                            'from'    => 'no-reply@my.phoenixcollege.edu',//overrides email from
                            'subject' => 'Your PC grades',
                        ],
                    ],
                    'print' => [
                        'view'    => 'kiosk.module.grade.print',
                        'name'    => 'Print',
                        'action'  => 'App\Http\Controllers\Kiosk\Module\GradeModule@getPrint',
                        'params'  => [
                            'term_id' => 'last_term_id',
                        ],
                        'options' => [
                            'attributes' => [
                                'target="_blank"',
                            ],
                        ],
                    ],
                ],
            ],
            'CardBalanceModule'  => [
                'code'       => 'CardBalanceModule',
                'name'       => 'My Student ID Balance',
                'controller' => 'App\Http\Controllers\Kiosk\Module\CardBalanceModule',
                'action'     => 'App\Http\Controllers\Kiosk\Module\CardBalanceModule@index',
                'icon'       => '',
                'base'       => 'balance',
                'order'      => 4,
                'routes'     => [
                    'index' => '/',
                ],
            ],
        ];
    }
}
