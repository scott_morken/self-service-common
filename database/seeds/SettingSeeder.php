<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/20/17
 * Time: 7:41 AM
 */

class SettingSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $this->createSettings();
    }

    protected function createSettings()
    {
        factory(\Smorken\Settings\Models\Eloquent\Setting::class)->create(
            [
                'key'   => 'student_center_link',
                'descr' => 'Student Center Link',
                'value' => 'https://csoprodpub.maricopa.edu/psp/PMCPAJ/STUDENT/HRMS/c/SA_LEARNER_SERVICES.SSS_STUDENT_CENTER.GBL',
            ]
        );
        factory(\Smorken\Settings\Models\Eloquent\Setting::class)->create(
            [
                'key'   => 'fa_todo_keys',
                'descr' => 'FA Todo Keys',
                'value' => '',
            ]
        );
    }
}
