<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:57 AM
 */
class CollegeSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $this->colleges();
    }

    protected function colleges()
    {
        factory(\Smorken\SSCommon\Models\Eloquent\College::class)->create(
            [
                'id'   => 'PCC01',
                'descr' => 'Phoenix College',
            ]
        );
    }
}
