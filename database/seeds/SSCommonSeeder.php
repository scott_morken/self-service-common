<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/20/17
 * Time: 7:56 AM
 */

class SSCommonSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $this->call('SettingSeeder');
        $this->call('ModuleSeeder');
        $this->call('AppletSeeder');
        $this->call('CollegeSeeder');
    }
}
