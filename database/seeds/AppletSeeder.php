<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:57 AM
 */
class AppletSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $this->coreApplets();
    }

    protected function coreApplets()
    {
        factory(\Smorken\SSCommon\Models\Eloquent\Applet::class)->create(
            [
                'code'        => 'holds',
                'name'        => 'Holds',
                'controller'  => 'App\Http\Controllers\Core\Applet\ServInd\ServIndApplet',
                'handler'     => 'App\Applet\ServInd\ServIndApplet',
                'provider'    => 'App\Contracts\Storage\Sis\ServiceIndicator',
                'applet_view' => 'core.applet.servind.applet',
                'base'        => '',
            ]
        );
        factory(\Smorken\SSCommon\Models\Eloquent\Applet::class)->create(
            [
                'code'        => 'messages',
                'name'        => 'Messages',
                'controller'  => 'App\Http\Controllers\Core\Applet\Message\MessageApplet',
                'handler'     => 'App\Applet\Message\MessageApplet',
                'provider'    => 'Smorken\SSCommon\Contracts\Storage\Applet\Message',
                'applet_view' => 'core.applet.message.applet',
                'base'        => '',
            ]
        );
        $links = factory(\Smorken\SSCommon\Models\Eloquent\Applet::class)->create(
            [
                'code'        => 'links',
                'name'        => 'Useful Links',
                'controller'  => 'App\Http\Controllers\Core\Applet\Link\LinkApplet',
                'handler'     => 'App\Applet\Link\LinkApplet',
                'provider'    => 'Smorken\SSCommon\Contracts\Storage\Applet\Link',
                'applet_view' => 'core.applet.link.applet',
                'base'        => 'link',
            ]
        );
        factory(\Smorken\SSCommon\Models\Eloquent\Route::class)->create(
            [
                'parent'    => 'APP',
                'parent_id' => $links->id,
                'action'      => 'index',
                'path'     => '/',
            ]
        );

        $todos = factory(\Smorken\SSCommon\Models\Eloquent\Applet::class)->create(
            [
                'code'        => 'todos',
                'name'        => 'My Todos',
                'controller'  => 'App\Http\Controllers\Core\Applet\Todo\TodoApplet',
                'handler'     => 'App\Applet\Todo\TodoApplet',
                'provider'    => 'App\Contracts\Storage\Sis\Todo',
                'applet_view' => 'core.applet.todo.applet',
                'base'        => 'todo',
            ]
        );
        factory(\Smorken\SSCommon\Models\Eloquent\Route::class)->create(
            [
                'parent'    => 'APP',
                'parent_id' => $todos->id,
                'action'      => 'index',
                'path'     => '/',
            ]
        );
    }
}
