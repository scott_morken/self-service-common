<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Generator;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(
    Smorken\SSCommon\Models\Applet\Eloquent\Message::class,
    function (Generator $faker) {
        return [
            'message'     => $faker->text,
            'description' => $faker->text(64),
            'active'      => true,
            'start_date'  => null,
            'end_date'    => null,
            'order'       => 0,
            'login_page'  => 0,
            'classes'     => 'alert alert-info',
            'container'   => 'div',
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Adminer::class,
    function (Generator $faker) {
        return [
            'id'         => $faker->unique()->randomNumber(8),
            'first_name' => $faker->firstName,
            'last_name'  => $faker->lastName,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Applet::class,
    function (Generator $faker) {
        return [
            'code'        => $faker->word,
            'name'        => $faker->text(128),
            'controller'  => $faker->text(128),
            'handler'     => $faker->text(128),
            'provider'    => $faker->text(128),
            'applet_view' => $faker->text(128),
            'base'        => $faker->text(32),
            'type'        => 'CORE',
            'active'      => true,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Route::class,
    function (Generator $faker) {
        return [
            'parent'    => $faker->randomElements(['APP', 'MOD']),
            'parent_id' => $faker->randomNumber(1),
            'action'    => $faker->text(32),
            'path'      => $faker->text(128),
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Module::class,
    function (Generator $faker) {
        return [
            'code'         => $faker->word,
            'name'         => $faker->text(128),
            'controller'   => $faker->text(128),
            'action'       => $faker->text(128),
            'base'         => $faker->text(32),
            'icon'         => $faker->text(32),
            'display_both' => true,
            'is_image'     => false,
            'type'         => 'CORE',
            'active'       => true,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\ActionLog::class,
    function (Generator $faker) {
        return [
            'user_id'    => $faker->randomNumber(8),
            'controller' => $faker->text(128),
            'action'     => $faker->word,
            'ip'         => $faker->ipv4,
            'kiosk'      => $faker->randomElement([0, 1]),
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\ModuleOutput::class,
    function (Generator $faker) {
        return [
            'module_id'    => $faker->randomNumber(2),
            'code'         => $faker->word,
            'name'         => $faker->text(128),
            'view'         => $faker->text(128),
            'action'       => $faker->text(128),
            'icon'         => $faker->text(32),
            'display_both' => true,
            'show_nav'     => true,
            'params'       => json_encode(['term_id' => 'term_id']),
            'options'      => json_encode(['from' => 'foo@example.org', 'subject' => 'test']),
            'active'       => true,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Lock::class,
    function (Generator $faker) {
        return [
            'user_id'  => $faker->randomNumber(8),
            'attempts' => 0,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Location::class,
    function (Generator $faker) {
        return [
            'org_id'          => $faker->randomNumber(8),
            'ext_location_id' => $faker->randomNumber(3),
            'descr'           => $faker->words(3, true),
            'latitude'        => null,
            'longitude'       => null,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\Org::class,
    function (Generator $faker) {
        return [
            'ext_org_id' => $faker->randomNumber(3),
            'descr'      => $faker->words(3, true),
            'latitude'   => null,
            'longitude'  => null,
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Eloquent\College::class,
    function (Generator $faker) {
        return [
            'id'    => $faker->randomNumber(3),
            'descr' => $faker->words(3, true),
        ];
    }
);

$factory->define(
    Smorken\SSCommon\Models\Applet\Eloquent\Link::class,
    function (Generator $faker) {
        return [
            'title'   => $faker->words(3, true),
            'href'    => $faker->url,
            'target'  => '_blank',
            'classes' => '',
            'active'  => 1,
            'weight'  => 0,
        ];
    }
);
