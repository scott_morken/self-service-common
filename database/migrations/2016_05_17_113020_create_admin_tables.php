<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('messages')) {
            Schema::create(
                'messages',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->text('message');
                    $t->string('description', 64);
                    $t->boolean('active')->default(true);
                    $t->dateTime('start_date')->nullable();
                    $t->dateTime('end_date')->nullable();
                    $t->unsignedSmallInteger('order')->default(0);
                    $t->boolean('login_page')->default(false);
                    $t->string('classes', 32)->default('');
                    $t->string('container', 5)->default('div');
                    $t->timestamps();

                    $t->index('active', 'msg_active_ndx');
                    $t->index('start_date', 'msg_start_date_ndx');
                    $t->index('end_date', 'msg_end_date_ndx');
                    $t->index('order', 'msg_order_ndx');
                    $t->index('login_page', 'msg_login_page_ndx');
                }
            );
        }
        if (!Schema::hasTable('adminers')) {
            Schema::create(
                'adminers',
                function (Blueprint $t) {
                    $t->integer('id')->unsigned();
                    $t->string('first_name', 32);
                    $t->string('last_name', 32);
                    $t->timestamps();

                    $t->primary('id');
                }
            );
        }
        if (!Schema::hasTable('colleges')) {
            Schema::create(
                'colleges',
                function (Blueprint $t) {
                    $t->string('id', 16);
                    $t->string('descr', 32);
                    $t->timestamps();

                    $t->primary('id');
                }
            );
        }
        if (!Schema::hasTable('applets')) {
            Schema::create(
                'applets',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->string('code', 32);
                    $t->string('name', 128);
                    $t->string('controller', 128);
                    $t->string('handler', 128);
                    $t->string('provider', 128);
                    $t->string('applet_view', 128);
                    $t->string('base', 32)->default('');
                    $t->string('type', 5)->default('CORE');
                    $t->mediumInteger('order')->unsigned()->default(0);
                    $t->boolean('active')->default(true);
                    $t->timestamps();

                    $t->index('active', 'app_active_ndx');
                    $t->index('type', 'app_type_ndx');
                    $t->index('order', 'app_order_ndx');
                    $t->unique(['code', 'type'], 'app_unique_ndx');
                }
            );
        }
        if (!Schema::hasTable('routes')) {
            Schema::create(
                'routes',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->string('parent', 3);
                    $t->integer('parent_id')->unsigned();
                    $t->string('action', 32);
                    $t->string('path', 128);
                    $t->timestamps();

                    $t->unique(['parent', 'parent_id', 'action'], 'rt_parent_action_ndx');
                    $t->index('parent_id', 'rt_parent_id_ndx');
                    $t->index('parent', 'rt_parent_ndx');
                }
            );
        }
        if (!Schema::hasTable('modules')) {
            Schema::create(
                'modules',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->string('code', 32);
                    $t->string('name', 128);
                    $t->string('controller', 128);
                    $t->string('action', 128);
                    $t->string('base', 32);
                    $t->string('icon', 32)->default('');
                    $t->boolean('display_both')->default(true);
                    $t->boolean('is_image')->default(false);
                    $t->string('type', 5)->default('CORE');
                    $t->mediumInteger('order')->unsigned()->default(0);
                    $t->boolean('active')->default(true);
                    $t->timestamps();

                    $t->index('active', 'mod_active_ndx');
                    $t->index('type', 'mod_type_ndx');
                    $t->index('order', 'mod_order_ndx');
                    $t->unique(['code', 'type'], 'mod_unique_ndx');
                }
            );
        }
        if (!Schema::hasTable('module_outputs')) {
            Schema::create(
                'module_outputs',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->integer('module_id')->unsigned();
                    $t->string('code', 32);
                    $t->string('name', 128);
                    $t->string('view', 128)->default('');
                    $t->string('action', 128);
                    $t->string('icon', 32)->default('');
                    $t->boolean('display_both')->default(true);
                    $t->boolean('show_nav')->default(true);
                    $t->string('params', 255)->default('');
                    $t->string('options', 255)->default('');
                    $t->boolean('active')->default(true);
                    $t->timestamps();

                    $t->index('module_id', 'modout_module_id_ndx');
                    $t->index('active', 'modout_active_ndx');
                    $t->unique(['module_id', 'code'], 'modout_unique_ndx');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'messages',
            'adminers',
            'applets',
            'colleges',
            'modules',
            'routes',
            'module_outputs',
        ];
        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }
}
