<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddKioskToActionLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    if (!Schema::hasColumn('self_service_log', 'kiosk')) {
            Schema::table(
                'self_service_log',
                function (Blueprint $t) {
                    $t->boolean('kiosk')->default(0);

                    $t->index('kiosk', 'ssl_kiosk_ndx');
                }
            );
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
