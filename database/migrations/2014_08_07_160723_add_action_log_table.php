<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddActionLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    if (!Schema::hasTable('self_service_log')) {
            Schema::create(
                'self_service_log',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->integer('user_id');
                    $t->string('controller', 64);
                    $t->string('action', 32);
                    $t->string('ip', 64);
                    $t->timestamps();

                    $t->index('ip', 'ssl_ip_ndx');
                    $t->index('user_id', 'ssl_user_id_ndx');
                }
            );
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('self_service_log');
	}

}
