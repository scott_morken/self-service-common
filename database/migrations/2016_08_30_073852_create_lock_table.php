<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateLockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('locks')) {
            Schema::create(
                'locks',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->integer('user_id');
                    $t->smallInteger('attempts')->default(0);
                    $t->timestamps();

                    $t->unique('user_id', 'lk_user_id_ndx');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locks');
    }
}
