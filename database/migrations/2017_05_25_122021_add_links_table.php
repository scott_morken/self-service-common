<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('applet_links')) {
            Schema::create(
                'applet_links',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->string('title', 64);
                    $t->string('href', 256);
                    $t->string('target', 16);
                    $t->string('classes', 64);
                    $t->boolean('active')->default(1);
                    $t->tinyInteger('weight')->default(0);
                    $t->timestamps();

                    $t->index('weight', 'applink_weight_ndx');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applet_links');
    }
}
