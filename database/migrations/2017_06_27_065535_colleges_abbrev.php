<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CollegesAbbrev extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colleges', function (Blueprint $t) {
            //$t->dropColumn('abbrev');
        });
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colleges', function (Blueprint $t) {
            $t->string('abbrev', 10)->default('');
            $conn = \Illuminate\Support\Facades\DB::connection($this->getConnection());
            $sql = 'ALTER TABLE colleges MODIFY descr VARCHAR(50);';
            if ($conn instanceof \Illuminate\Database\SqlServerConnection) {
                $sql = 'ALTER TABLE colleges ALTER COLUMN descr VARCHAR(50);';
            }
            $conn->raw($sql);
        });
    }
}
