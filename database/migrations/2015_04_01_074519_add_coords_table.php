<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoordsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('orgs')) {
        Schema::create(
            'orgs',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('ext_org_id', 16);
                $t->string('descr', 64);
                $t->decimal('latitude', 9, 6)->nullable();
                $t->decimal('longitude', 9, 6)->nullable();
                $t->timestamps();

                $t->unique('ext_org_id', 'o_ext_org_id_ndx');
            }
        );
        }
        if (!Schema::hasTable('locations')) {
            Schema::create(
                'locations',
                function (Blueprint $t) {
                    $t->increments('id');
                    $t->integer('org_id')->unsigned();
                    $t->string('ext_location_id', 16);
                    $t->string('descr', 64);
                    $t->decimal('latitude', 9, 6)->nullable();
                    $t->decimal('longitude', 9, 6)->nullable();
                    $t->timestamps();

                    $t->unique('ext_location_id', 'l_ext_location_id_ndx');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orgs');
        Schema::dropIfExists('locations');
    }

}
