<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/24/16
 * Time: 11:01 AM
 */

namespace Smorken\SSCommon;

use Illuminate\Support\ServiceProvider as SP;
use League\CommonMark\Converter;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\SSCommon\Renderers\Markdown;

class ServiceProvider extends SP
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootFactory();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->bootConfig();
        $this->bootViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Smorken\Settings\ServiceProvider::class);
        $this->app->register(\GrahamCampbell\Markdown\MarkdownServiceProvider::class);
        $this->app->bind(
            \Smorken\SSCommon\Contracts\Renderers\Render::class,
            function ($app) {
                return new Markdown($app[Converter::class], $app[Sanitize::class]);
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'sscommon');
        $this->publishes([$config => config_path('sscommon.php')], 'config');
    }

    protected function bootFactory()
    {
        if ($this->app->bound('Illuminate\Database\Eloquent\Factory') && class_exists('Faker\Generator')) {
            $this->app->make('Illuminate\Database\Eloquent\Factory')
                      ->load(__DIR__.'/../database/factories');
        }
    }

    protected function bootViews()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/sscommon');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('views/vendor/smorken/sscommon'),
            ],
            'views'
        );
    }
}
