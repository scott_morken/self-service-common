<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:20 AM
 */

namespace Smorken\SSCommon\Importers;

use Illuminate\Support\Arr;
use Smorken\SSCommon\Contracts\Import\Import;
use Smorken\SSCommon\Contracts\Storage\Location;
use Smorken\SSCommon\Contracts\Storage\Org;

class Csv implements Import
{

    /**
     * @var Org
     */
    protected $orgProvider;
    /**
     * @var Location
     */
    protected $locProvider;
    protected $delimiter;
    protected $enclosure;
    protected $escape;
    protected $fields = [
        'ext_org_id',
        'ext_location_id',
        'descr',
        'latitude',
        'longitude',
    ];

    protected $errors = [];

    protected $org_id_map = null;

    public function __construct(
        Org $orgProvider,
        Location $locProvider,
        $delimiter = ',',
        $enclosure = '"',
        $escape = "\\"
    ) {
        $this->orgProvider = $orgProvider;
        $this->locProvider = $locProvider;
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }

    public function import($filename)
    {
        $fh = fopen($filename, 'r');
        $rows = 0;
        if ($fh) {
            $this->createOrgIdMap();
            $field_count = count($this->fields);
            while (($data = fgetcsv($fh, 0, $this->delimiter, $this->enclosure, $this->escape)) !== false) {
                if (count($data) == $field_count) {
                    $r = $this->locProvider->import($this->parseData($data));
                    if (!$r) {
                        $this->addError($data);
                    } else {
                        $rows++;
                    }
                } else {
                    $this->addError($data);
                }
            }
            fclose($fh);
            unlink($filename);
        }
        return $rows;
    }

    protected function createOrgIdMap()
    {
        if (!$this->org_id_map) {
            $this->org_id_map = $this->orgProvider->getExtIdToIdMap();
        }
        return $this->org_id_map;
    }

    protected function parseData($data)
    {
        $parsed = [];
        foreach ($data as $i => $v) {
            $parsed[$this->fields[$i]] = $v;
        }
        $org_id = Arr::get($this->createOrgIdMap(), $parsed['ext_org_id'], null);
        if (!$org_id) {
            return false;
        }
        $parsed['org_id'] = $org_id;
        unset($parsed['ext_org_id']);
        return $parsed;
    }

    protected function addError($data)
    {
        $this->errors[] = implode($this->delimiter, $data);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return count($this->errors) > 0;
    }
}
