<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/17
 * Time: 12:33 PM
 */

namespace Smorken\SSCommon\Storage\Applet\Eloquent;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Smorken\SSCommon\Storage\Eloquent\Base;

class Link extends Base implements \Smorken\SSCommon\Contracts\Storage\Applet\Link
{

    /**
     * Return active links
     * @return Collection<\Smorken\Sscommon\Contracts\Models\Applet\Link>
     */
    public function load()
    {
        $key = 'links.active';
        return Cache::remember(
            $key,
            60 * 60,
            function () {
                return $this->getModel()
                            ->newQuery()
                            ->active()
                            ->orderWeight()
                            ->get();
            }
        );
    }
}
