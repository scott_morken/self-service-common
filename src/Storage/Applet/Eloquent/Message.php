<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:07 PM
 */

namespace Smorken\SSCommon\Storage\Applet\Eloquent;

use Smorken\SSCommon\Storage\Eloquent\Base;

class Message extends Base implements \Smorken\SSCommon\Contracts\Storage\Applet\Message
{

    /**
     * @param bool $login_page
     * @return \Smorken\SSCommon\Contracts\Models\Applet\Message[]
     */
    public function activeMessages($login_page = false)
    {
        return $this->getModel()
                    ->newQuery()
                    ->active($login_page)
                    ->orderBy('order')
                    ->orderBy('updated_at', 'DESC')
                    ->get();
    }
}
