<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 9:31 AM
 */

namespace Smorken\SSCommon\Storage\Traits;

use Illuminate\Support\Collection;

trait Module
{

    public function getRunnable($type = \Smorken\SSCommon\Contracts\Models\Module::CORE)
    {
        $runnable = new Collection();
        foreach ($this->getModules($type) as $m) {
            if ($r = $this->isRunnable($m)) {
                $runnable->put($m->code, $m);
            }
        }
        return $runnable;
    }

    public function isRunnable($model)
    {
        return ($model && $model->name && $model->action);
    }
}
