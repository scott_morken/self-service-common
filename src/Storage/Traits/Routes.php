<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 2:22 PM
 */

namespace Smorken\SSCommon\Storage\Traits;

use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class Routes
 *
 * @package App\Storage\Traits
 *
 * Helper for creating routes for modules and applets
 */
trait Routes
{

    /**
     * @var Collection
     */
    protected $registered;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @return Collection
     */
    public function getRegisteredRoutes()
    {
        if (!$this->registered) {
            $this->registered = new Collection();
        }
        return $this->registered;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->router;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function getRoutesFrom($model, $type, $prefix = null)
    {
        $this->ensureTypeSet($type);
        $routes_coll = new Collection();
        if ($model && $model->routes) {
            foreach ($model->routes as $route) {
                $routes_coll->put($route->action, $route);
            }
        }
        $this->registerRoutes($model, $routes_coll, $type, $prefix);
        return $this->getRegisteredRoutes()->get($type);
    }

    public function registerRoutes($model, $routes, $type, $prefix = null)
    {
        $qual_routes = $this->qualifyRoutes($routes, $model);
        $this->ensureTypeSet($type);
        $registered = $this->getRegisteredRoutes()->get($type);
        foreach ($qual_routes as $action => $route_detail) {
            $method = $route_detail['method'];
            $fragment = $route_detail['fragment'];
            if (!$registered->has($action)) {
                $this->registerRoute($action, $method, $fragment, $type, $prefix);
            }
        }
        return $this->getRegisteredRoutes();
    }

    protected function buildRoute($action, $method, $fragment, $prefix, $middleware)
    {
        $route = $this->getRouter()->middleware($middleware);
        if ($prefix) {
            $route = $route->prefix($prefix);
        }
        $route = $route->$method($fragment, $action);
//        $route->group(function () use ($action, $method, $fragment) {
//            $this->getRouter()->$method($fragment, $action);
//        });
        return $route;
    }

    protected function combineBaseAndPath($base, $path)
    {
        $base = $base ? Str::finish($base, '/') : null;
        if ($base && strpos($path, '/') === 0) {
            $path = substr($path, 1);
        }
        return $base.$path;
    }

    protected function ensureTypeSet($type)
    {
        if (!$this->getRegisteredRoutes()->get($type)) {
            $this->getRegisteredRoutes()->put($type, new Collection());
        }
    }

    protected function getKeyFrom($model, $key)
    {
        return (is_array($model) ? ($model[$key] ?? null) : $model->$key);
    }

    protected function getMethodFromPath($path)
    {
        $expPath = explode('@', $path);
        if (count($expPath) === 2) {
            return $expPath[0];
        }
        return 'get';
    }

    protected function getPathFromPath($path)
    {
        $expPath = explode('@', $path);
        return last($expPath);
    }

    protected function qualifyRoutes($routes, $model)
    {
        $qualified = [];
        $base = $this->getKeyFrom($model, 'base');
        $controller = $this->getKeyFrom($model, 'controller');
        if ($controller) {
            foreach ($routes as $route) {
                $action = $route->action;
                $path = $route->path;
                $method = $this->getMethodFromPath($path);
                $fragment = $this->getPathFromPath($path);
                $qualified[$controller.'@'.$action] = [
                    'method' => $method,
                    'fragment' => $this->combineBaseAndPath($base, $fragment),
                ];
            }
        }
        return $qualified;
    }

    protected function registerRoute($action, $method, $fragment, $type, $prefix)
    {
        //handle store action in the controller itself ($this->middleware('store.action'))
        $mw = $type === 'KIOSK' ? ['web', 'user.loaded', 'verify.client'] : ['web', 'auth'];
        $route = $this->buildRoute($action, $method, $fragment, $prefix, $mw);
        $this->ensureTypeSet($type);
        $c = $this->getRegisteredRoutes()->get($type);
        $c->put($action, $route);
    }
}
