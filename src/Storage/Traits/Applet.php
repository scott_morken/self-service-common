<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 9:31 AM
 */

namespace Smorken\SSCommon\Storage\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

trait Applet
{

    public function getRunnable($type = \Smorken\SSCommon\Contracts\Models\Applet::CORE)
    {
        $runnable = new Collection();
        foreach ($this->getApplets($type) as $a) {
            if ($r = $this->isRunnable($a)) {
                $a->object = $r;
                $runnable->put($a->code, $a);
            }
        }
        return $runnable;
    }

    /**
     * @param $model
     * @return \Smorken\SSCommon\Contracts\Output\Applet|false
     */
    public function isRunnable($model)
    {
        if (!$model) {
            return false;
        }
        $fqn = $this->getFqn($model, $model->code);
        $o = new $fqn();
        $o->setSourceModel($model);
        $repo = $this->getProviderFrom($model);
        if ($repo) {
            $o->setProvider(App::make($repo));
        }
        return $o;
    }

    /**
     * @param $model
     * @param $name
     * @return string
     */
    protected function getFqn($model, $name)
    {
        $fqn = $model->handler;
        if (!$fqn) {
            $fqn = "App\\Applet\\$name";
        }
        return $fqn;
    }

    protected function getProviderFrom($model)
    {
        return $model->provider;
    }
}
