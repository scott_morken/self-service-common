<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 12:36 PM
 */

namespace Smorken\SSCommon\Storage\Config;

class Adminer implements \Smorken\SSCommon\Contracts\Storage\Adminer
{

    public function firstOrCreateById($id, array $attributes)
    {
        // not used
    }

    /**
     * @param $id
     * @return bool
     */
    public function isAllowed($id)
    {
        $users = ($this->getModel()['users'] ?? []);
        if (in_array($id, $users)) {
            return true;
        }
        return false;
    }
}
