<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/4/14
 * Time: 3:07 PM
 */

namespace Smorken\SSCommon\Storage\Config;

use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\SSCommon\Contracts\Models\ModuleOutput;
use Smorken\SSCommon\Contracts\Models\Route;
use Smorken\SSCommon\Storage\Traits\Routes;

class Module implements \Smorken\SSCommon\Contracts\Storage\Module
{

    use Routes, \Smorken\SSCommon\Storage\Traits\Module;

    protected $config = [];

    /**
     * @var \Smorken\SSCommon\Contracts\Models\Module
     */
    protected $model;

    /**
     * @var Collection
     */
    protected $modules;

    /**
     * @var ModuleOutput
     */
    protected $outputModel;

    /**
     * @var Route
     */
    protected $routeModel;

    public function __construct(
        \Smorken\SSCommon\Contracts\Models\Module $model,
        Route $routeModel,
        ModuleOutput $outputModel,
        Router $router,
        $config
    ) {
        $this->model = $model;
        $this->routeModel = $routeModel;
        $this->outputModel = $outputModel;
        $this->setRouter($router);
        $this->config = $config;
    }

    public function active()
    {
        return $this->getModules()->merge($this->getModules(\Smorken\SSCommon\Contracts\Models\Applet::KIOSK));
    }

    /**
     * @param $name
     * @return \Smorken\SSCommon\Contracts\Models\Module
     */
    public function getModule($name)
    {
        $modules = $this->getModules();
        return $modules->get($name, null);
    }

    public function getModules($type = \Smorken\SSCommon\Contracts\Models\Module::CORE)
    {
        $modules = new Collection();
        if (!$this->modules) {
            $m = ($this->config['active'] ?? []);
            foreach ($m as $k => $v) {
                if (!is_array($v)) {
                    $v = (array) $v;
                }
                $model = $this->createModelFrom($k, $v, $type, true);
                $modules->put($k, $model);
            }
            $this->modules = $modules;
        }
        return $this->modules;
    }

    public function getOutputsFrom($config)
    {
        $coll = new Collection();
        $outputs = ($config['output'] ?? []);
        foreach ($outputs as $id => $data) {
            $data['id'] = $id;
            $coll->put($id, $this->outputModel->newInstance($data));
        }
        return $coll;
    }

    public function getRoutesFrom($config, $type, $prefix = null)
    {
        $routes_coll = new Collection();
        if ($config && isset($config['routes'])) {
            foreach ($config['routes'] as $action => $path) {
                $routes_coll->put(
                    $action,
                    $this->routeModel->newInstance(
                        [
                            'action' => $action,
                            'path' => $path,
                        ]
                    )
                );
            }
        }
        $this->registerRoutes($config, $routes_coll, $type, $prefix);
        return $routes_coll;
    }

    protected function createModelFrom($id, $config, $type, $active)
    {
        $config['id'] = $id;
        $config['active'] = $active;
        $config['base'] = $this->getBaseFromConfig($config, $id);
        $config['routes'] = $this->getRoutesFrom($config, $type, 'module');
        $config['outputs'] = $this->getOutputsFrom($config);
        return $this->model->newInstance($config);
    }

    protected function getBaseFromConfig($config, $name)
    {
        if (isset($config['base'])) {
            return $config['base'];
        }
        return Str::slug(substr($name, 0, strpos($name, 'Module')));
    }
}
