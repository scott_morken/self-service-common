<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/4/14
 * Time: 3:07 PM
 */

namespace Smorken\SSCommon\Storage\Config;

use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\SSCommon\Contracts\Models\Route;
use Smorken\SSCommon\Storage\Traits\Routes;

class Applet implements \Smorken\SSCommon\Contracts\Storage\Applet
{

    use Routes, \Smorken\SSCommon\Storage\Traits\Applet;

    protected $applets;

    protected $config = [];

    /**
     * @var \Smorken\SSCommon\Contracts\Models\Applet
     */
    protected $model;

    /**
     * @var Route
     */
    protected $routeModel;

    public function __construct(
        \Smorken\SSCommon\Contracts\Models\Applet $model,
        Route $routeModel,
        Router $router,
        array $config = []
    ) {
        $this->model = $model;
        $this->routeModel = $routeModel;
        $this->setRouter($router);
        $this->config = $config;
    }

    public function getApplet($name)
    {
        $applets = $this->getApplets();
        return $applets->get($name, null);
    }

    public function active()
    {
        return $this->getApplets()->merge($this->getApplets(\Smorken\SSCommon\Contracts\Models\Applet::KIOSK));
    }

    public function getApplets($type = \Smorken\SSCommon\Contracts\Models\Applet::CORE)
    {
        $applets = new Collection();
        if (!$this->applets) {
            $m = ($this->config['active'] ?? []);
            foreach ($m as $k => $v) {
                if (!is_array($v)) {
                    $v = (array) $v;
                }
                $model = $this->createModelFrom($k, $v, $type, true);
                $applets->put($k, $model);
            }
            $this->applets = $applets;
        }
        return $this->applets;
    }

    public function getRoutesFrom($config, $type, $prefix = null)
    {
        $routes_coll = new Collection();
        if ($config && isset($config['routes'])) {
            foreach ($config['routes'] as $action => $path) {
                $routes_coll->put(
                    $action,
                    $this->routeModel->newInstance(
                        [
                            'action' => $action,
                            'path' => $path,
                        ]
                    )
                );
            }
        }
        $this->registerRoutes($config, $routes_coll, $type, $prefix);
        return $routes_coll;
    }

    protected function createModelFrom($id, $config, $type, $active)
    {
        $config['id'] = $id;
        $config['code'] = $id;
        $config['active'] = $active;
        $config['name'] = $this->getName($config);
        $config['base'] = $this->getBaseFromConfig($config, $id);
        $config['routes'] = $this->getRoutesFrom($config, $type, 'applet');
        return $this->model->newInstance($config);
    }

    protected function getBaseFromConfig($config, $name)
    {
        if (isset($config['base'])) {
            return $config['base'];
        }
        return Str::slug(substr($name, 0, strpos($name, 'Applet')));
    }

    protected function getName($config)
    {
        if (isset($config['name'])) {
            return $config['name'];
        }
        return $config['code'];
    }
}
