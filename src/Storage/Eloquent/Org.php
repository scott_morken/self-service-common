<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:18 AM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class Org extends Base implements \Smorken\SSCommon\Contracts\Storage\Org
{

    public function getExtIdToIdMap()
    {
        return $this->getModel()->newQuery()->pluck('id', 'ext_org_id');
    }

    /**
     * Update an existing entity
     *
     * @param  $model
     * @param  array  $input
     * @return mixed
     */
    public function update($model, array $input)
    {
        $model->fill($input);
        if ($model->save()) {
            return $model;
        }
        return false;
    }
}
