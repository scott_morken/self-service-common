<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:49 AM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

use Illuminate\Support\Facades\Cache;

class College extends Base implements \Smorken\SSCommon\Contracts\Storage\College
{

    /**
     * @param $id
     * @return \Smorken\SSCommon\Contracts\Models\College
     */
    public function getById($id)
    {
        $key = 'college.' . $id;
        return Cache::remember(
            $key,
            60 * 10,
            function () use ($id) {
                return $this->getModel()
                            ->newQuery()
                            ->where('id', '=', $id)
                            ->first();
            }
        );
    }
}
