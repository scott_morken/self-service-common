<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:06 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Cache;
use Smorken\SSCommon\Storage\Traits\Routes;

class Applet extends Base implements \Smorken\SSCommon\Contracts\Storage\Applet
{

    use Routes, \Smorken\SSCommon\Storage\Traits\Applet;

    protected $applets;

    public function __construct($model, Router $router)
    {
        $this->setRouter($router);
        parent::__construct($model);
    }

    public function active()
    {
        $key = 'applets.active';
        return Cache::remember(
            $key,
            60 * 60,
            function () {
                return $this->getModel()
                            ->where('active', '=', 1)
                            ->orderBy('type')
                            ->orderBy('order')
                            ->orderBy('name')
                            ->get();
            }
        );
    }

    public function getApplet($name)
    {
        $applets = $this->getApplets();
        return $applets->get($name, null);
    }

    public function getApplets($type = \Smorken\SSCommon\Contracts\Models\Applet::CORE)
    {
        $key = "applets.$type";
        if (!$this->applets) {
            $this->applets = Cache::remember(
                $key,
                60 * 60,
                function () use ($type) {
                    $coll = $this->getModel()
                                 ->newQuery()
                                 ->with('routes')
                                 ->where('active', '=', 1)
                                 ->where('type', '=', $type)
                                 ->orderBy('order')
                                 ->orderBy('name')
                                 ->get();
                    return $coll->keyBy('code');
                }
            );
            foreach ($this->applets as $applet) {
                $this->getRoutesFrom($applet, $type, 'applet');
            }
        }
        return $this->applets;
    }
}
