<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:06 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class Adminer extends Base implements \Smorken\SSCommon\Contracts\Storage\Adminer
{

    public function firstOrCreateById($id, array $attributes)
    {
        $exists = $this->getModel()->where('id', '=', $id)->exists();
        if (!$exists) {
            $attributes['id'] = $id;
            $m = $this->getModel()->fill($attributes);
            return $m->save();
        }
        return $exists;
    }

    /**
     * @param $id
     * @return bool
     */
    public function isAllowed($id)
    {
        return $this->getModel()
                    ->newQuery()
                    ->where('id', '=', $id)
                    ->exists();
    }
}
