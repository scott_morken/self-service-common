<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 7:57 AM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class Lock extends Base implements \Smorken\SSCommon\Contracts\Storage\Lock
{

    /**
     * @param $user_id
     * @return int
     */
    public function clear($user_id)
    {
        return $this->getModel()->where('user_id', '=', $user_id)->delete();
    }

    /**
     * @param $user_id
     * @return int
     */
    public function incrementFailures($user_id)
    {
        $m = $this->getModel()->firstOrNew(['user_id' => $user_id]);
        return $m->incrementFailures();
    }

    /**
     * @param $user_id
     * @param $max_count
     * @param $max_time
     * @return bool
     */
    public function isLocked($user_id, $max_count, $max_time)
    {
        $m = $this->getModel()->where('user_id', '=', $user_id)->first();
        return $m && $m->isLocked($max_count, $max_time);
    }

    protected function filterUserId($q, $v)
    {
        if (strlen($v)) {
            $q = $q->userIdIs($v);
        }
        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'user_id' => 'filterUserId',
        ];
    }
}
