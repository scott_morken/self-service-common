<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:07 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Cache;
use Smorken\SSCommon\Storage\Traits\Routes;

class Module extends Base implements \Smorken\SSCommon\Contracts\Storage\Module
{

    use Routes, \Smorken\SSCommon\Storage\Traits\Module;

    protected $modules;

    public function __construct($model, Router $router)
    {
        $this->setRouter($router);
        parent::__construct($model);
    }

    public function active()
    {
        $key = 'modules.active';
        return Cache::remember(
            $key,
            60 * 60,
            function () {
                return $this->getModel()
                            ->where('active', '=', 1)
                            ->orderBy('type')
                            ->orderBy('order')
                            ->orderBy('name')
                            ->get();
            }
        );
    }

    public function getModule($name)
    {
        $modules = $this->getModules();
        $name = $this->cleanControllerName($name);
        foreach ($modules as $module) {
            $mc = $this->cleanControllerName($module->controller);
            if ($mc === $name) {
                return $module;
            }
        }
        return null;
    }

    public function getModules($type = \Smorken\SSCommon\Contracts\Models\Module::CORE)
    {
        $key = "modules.$type";
        if (!$this->modules) {
            $this->modules = Cache::remember(
                $key,
                60 * 60,
                function () use ($type) {
                    $coll = $this->getModel()
                                 ->newQuery()
                                 ->with('routes')
                                 ->with('outputs')
                                 ->where('active', '=', 1)
                                 ->where('type', '=', $type)
                                 ->orderBy('order')
                                 ->orderBy('name')
                                 ->get();
                    return $coll;
                }
            );
            foreach ($this->modules as $module) {
                $this->getRoutesFrom($module, $type, 'module');
                $module->routes = $module->routes->keyBy('action');
                $module->outputs = $module->outputs->keyBy('code');
            }
        }
        return $this->modules;
    }

    protected function cleanControllerName($name)
    {
        if (substr($name, 0, 1) === '\\') {
            return substr($name, 1);
        }
        return $name;
    }
}
