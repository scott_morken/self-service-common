<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:07 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class ModuleOutput extends Base implements \Smorken\SSCommon\Contracts\Storage\ModuleOutput
{

    protected function filterModuleId($q, $v)
    {
        if (strlen($v)) {
            $q = $q->moduleIdIs($v);
        }
        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'module_id' => 'filterModuleId',
        ];
    }
}
