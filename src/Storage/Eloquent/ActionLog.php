<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/7/14
 * Time: 4:20 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class ActionLog extends Base implements \Smorken\SSCommon\Contracts\Storage\ActionLog
{

    public function store(array $data)
    {
        $m = $this->getModel()->newInstance($data);
        return $m->save();
    }
}
