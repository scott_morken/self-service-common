<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 1:08 PM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

class Route extends Base implements \Smorken\SSCommon\Contracts\Storage\Route
{

    protected function filterParentId($q, $v)
    {
        if (strlen($v)) {
            $q = $q->parentIdIs($v);
        }
        return $q;
    }

    protected function filterType($q, $v)
    {
        if (strlen($v)) {
            $q = $q->parentTypeIs($v);
        }
        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'parent_id' => 'filterParentId',
            'type' => 'filterType',
        ];
    }
}
