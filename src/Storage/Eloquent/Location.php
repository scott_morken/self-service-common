<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:14 AM
 */

namespace Smorken\SSCommon\Storage\Eloquent;

use Illuminate\Support\Facades\Cache;

class Location extends Base implements \Smorken\SSCommon\Contracts\Storage\Location
{

    public function getByOrgId($org_id)
    {
        $key = 'locations.org_id.'.$org_id;
        return Cache::remember(
            $key,
            60 * 10,
            function () use ($org_id) {
                return $this->getModel()->newQuery()
                            ->where('org_id', $org_id)
                            ->where('latitude', '>', 0)
                            ->whereNotNull('latitude')
                            ->orderBy('descr')
                            ->get();
            }
        );
    }

    public function import(array $data)
    {
        if (!$data) {
            return false;
        }
        return $this->model->createOrUpdate(['ext_location_id'], $data);
    }

    /**
     * Update an existing entity
     *
     * @param    $model
     * @param  array  $input
     * @return mixed
     */
    public function update($model, array $input)
    {
        $model->fill($input);
        if ($model->save()) {
            return $model;
        }
        return false;
    }

    protected function getFilterMethods(): array
    {
        return [
            'descr' => 'filterDescrLike',
            'org_id' => 'filterOrgIdIs',
        ];
    }

    protected function filterDescrLike($q, $v)
    {
        if (strlen($v)) {
            $q = $q->descrLike($v);
        }
        return $q;
    }

    protected function filterOrgIdIs($q, $v)
    {
        if (strlen($v)) {
            $q = $q->orgIdIs($v);
        }
        return $q;
    }
}
