<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/24/16
 * Time: 11:12 AM
 */

namespace Smorken\SSCommon\Renderers;

use League\CommonMark\Converter;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\SSCommon\Contracts\Renderers\Render;

class Markdown implements Render
{

    /**
     * @var Converter
     */
    protected $converter;

    /**
     * @var Sanitize
     */
    protected $sanitize;

    public function __construct(Converter $converter, Sanitize $sanitize)
    {
        $this->converter = $converter;
        $this->sanitize = $sanitize;
    }

    public function render($data)
    {
        return $this->sanitize->xssAdmin($this->converter->convertToHtml($data));
        //return $this->converter->convertToHtml($data);
    }
}
