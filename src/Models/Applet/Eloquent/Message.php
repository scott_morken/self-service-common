<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:04 AM
 */

namespace Smorken\SSCommon\Models\Applet\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Smorken\SSCommon\Models\Eloquent\Base;

class Message extends Base implements \Smorken\SSCommon\Contracts\Models\Applet\Message
{

    protected $fillable = [
        'message',
        'description',
        'active',
        'start_date',
        'end_date',
        'order',
        'login_page',
        'classes',
        'container',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'message' => 'Message',
        'description' => 'Description',
        'active' => 'Active',
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'order' => 'Order',
        'login_page' => 'Show on Login',
        'classes' => 'Classes',
        'container' => 'Container',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'message' => 'required',
        'description' => 'required|max:64',
        'active' => 'required|boolean',
        'start_date' => 'nullable|date',
        'end_date' => 'nullable|date',
        'order' => 'required|integer|min:0',
        'login_page' => 'required|boolean',
        //classes and container are added in ::rules()
    ];

    public function asSelect($items)
    {
        $select = [];
        foreach ($items as $k => $v) {
            $select[$v] = $v;
        }
        return $select;
    }

    public function getAllowedClasses()
    {
        return config(
            'sscommon::config.messages.classes',
            [
                'alert alert-danger',
                'alert alert-warning',
                'alert alert-success',
                'alert alert-info',
            ]
        );
    }

    public function getAllowedContainers()
    {
        return config(
            'sscommon.messages.containers',
            [
                'div',
                'p',
                'small',
                'span',
            ]
        );
    }

    public function getClassesAttribute()
    {
        $classes = Arr::get($this->attributes, 'classes', null);
        if (in_array($classes, $this->getAllowedClasses(), true)) {
            return $classes;
        }
        return null;
    }

    public function getContainerAttribute()
    {
        $container = Arr::get($this->attributes, 'container', 'div');
        if (in_array($container, $this->getAllowedContainers(), true)) {
            return $container;
        }
        return 'div';
    }

    public function getEndDateAttribute()
    {
        return $this->getDate('end_date');
    }

    public function getStartDateAttribute()
    {
        return $this->getDate('start_date');
    }

    public function name()
    {
        return sprintf('[%d] %s', $this->id, $this->description);
    }

    public function rules()
    {
        $base = $this->rules;
        $base['classes'] = sprintf('in:%s', implode(',', $this->getAllowedClasses()));
        $base['container'] = sprintf('in:%s', implode(',', $this->getAllowedContainers()));
        return $base;
    }

    public function scopeActive($query, $login_page)
    {
        $date = date('Y-m-d H:i:s');
        $query->where(
            function ($q) use ($date) {
                $q->where('start_date', '<=', $date)
                  ->orWhereNull('start_date');
            }
        )
              ->where(
                  function ($q) use ($date) {
                      $q->where('end_date', '>=', $date)
                        ->orWhereNull('end_date');
                  }
              )
              ->where('active', '=', 1);
        if ($login_page) {
            $query->where('login_page', '=', 1);
        }
        return $query;
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('active', 'desc')
                     ->orderBy('start_date', 'desc')
                     ->orderBy('order');
    }

    public function validationRules(array $override = []): array
    {
        return $this->mergeValidationRules($override, $this->rules());
    }

    protected function getDate($which)
    {
        $raw = Arr::get($this->attributes, $which);
        return $raw ? Carbon::parse($raw) : null;
    }
}
