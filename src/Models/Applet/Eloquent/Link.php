<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/17
 * Time: 12:28 PM
 */

namespace Smorken\SSCommon\Models\Applet\Eloquent;

use Smorken\SSCommon\Models\Eloquent\Base;

class Link extends Base implements \Smorken\SSCommon\Contracts\Models\Applet\Link
{

    protected $fillable = ['title', 'href', 'target', 'classes', 'active', 'weight'];

    protected $friendly_columns = [
        'id' => 'ID',
        'title' => 'Title',
        'href' => 'Link',
        'target' => 'Target',
        'classes' => 'Classes',
        'active' => 'Active',
        'weight' => 'Weight',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'title' => 'required|max:64',
        'href' => 'required|url|max:256',
        'target' => 'required|in:_blank,_self,_parent,_top',
        'classes' => 'max:64',
        'active' => 'boolean',
        'weight' => 'int|max:99',
    ];

    protected $table = 'applet_links';

    public function scopeActive($query)
    {
        return $query->where('active', '=', 1);
    }

    public function scopeDefaultOrder($query)
    {
        return $this->scopeOrderWeight($query);
    }

    public function scopeOrderWeight($query)
    {
        return $query->orderBy('weight')
                     ->orderBy('title');
    }
}
