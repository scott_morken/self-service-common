<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 7:48 AM
 */

namespace Smorken\SSCommon\Models\VO;

class ModuleOutput extends VO implements \Smorken\SSCommon\Contracts\Models\ModuleOutput
{

    public function getOption($key, $default = null)
    {
        return $this->getFrom('options', $key, $default);
    }

    public function getParam($key, $default = null)
    {
        return $this->getFrom('params', $key, $default);
    }

    protected function getFrom($what, $key, $default)
    {
        $options = ($this->attributes[$what] ?? []);
        $v = ($options[$key] ?? null);
        if ($v === null) {
            $v = ($this->attributes[$key] ?? $default);
        }
        return $v;
    }
}
