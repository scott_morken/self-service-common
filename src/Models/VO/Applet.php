<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/16
 * Time: 7:47 AM
 */

namespace Smorken\SSCommon\Models\VO;

use Illuminate\Support\Collection;

class Applet extends VO implements \Smorken\SSCommon\Contracts\Models\Applet
{

    public function routes()
    {
        return ($this->attributes['routes'] ?? new Collection());
    }
}
