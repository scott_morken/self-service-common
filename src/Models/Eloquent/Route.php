<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:34 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class Route extends Base implements \Smorken\SSCommon\Contracts\Models\Route
{

    protected $fillable = [
        'parent',
        'parent_id',
        'action',
        'path',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'parent' => 'Parent',
        'parent_id' => 'Parent ID',
        'action' => 'Action',
        'path' => 'Path',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'parent' => 'required|in:APP,MOD',
        'parent_id' => 'required|integer|min:1',
        'action' => 'required|max:32',
        'path' => 'required|max:128',
    ];

    public function getParent()
    {
        $parent_cls = $this->parent === 'APP' ? Applet::class : Module::class;
        return $this->belongsTo($parent_cls, 'parent_id');
    }

    public function name()
    {
        return sprintf('[%d:%s] %s', $this->id, $this->parent, $this->action);
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('parent')
                     ->orderBy('parent_id')
                     ->orderBy('action');
    }

    public function scopeDefaultWiths($query)
    {
        return $query->with('getParent');
    }

    public function scopeParentIdIs($query, $value)
    {
        return $query->where('parent_id', '=', $value);
    }

    public function scopeParentTypeIs($query, $value)
    {
        return $query->where('parent', '=', $value);
    }
}
