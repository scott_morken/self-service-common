<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:13 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

use Illuminate\Validation\Rule;

class Org extends Base implements \Smorken\SSCommon\Contracts\Models\Org
{

    protected $fillable = [
        'ext_org_id',
        'descr',
        'latitude',
        'longitude',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'ext_org_id' => 'External ID',
        'descr' => 'Description',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'ext_org_id' => 'required|unique:orgs,ext_org_id',
        'descr' => 'required',
        'latitude' => 'nullable|numeric|required_with:longitude',
        'longitude' => 'nullable|numeric|required_with:latitude',
    ];

    public function getLatitudeAttribute()
    {
        $l = (float) ($this->attributes['latitude'] ?? null);
        return $l ?: null;
    }

    public function getLongitudeAttribute()
    {
        $l = (float) ($this->attributes['longitude'] ?? null);
        return $l ?: null;
    }

    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    public function name()
    {
        return $this->descr;
    }

    public function rules()
    {
        return [
            'ext_org_id' => ['required', Rule::unique($this->getTable())],
            'descr' => 'required',
            'latitude' => 'nullable|numeric|required_with:longitude',
            'longitude' => 'nullable|numeric|required_with:latitude',
        ];
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('descr');
    }

    public function validationRules(array $override = []): array
    {
        return $this->mergeValidationRules($override, $this->rules());
    }
}
