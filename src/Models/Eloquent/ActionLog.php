<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/7/14
 * Time: 4:18 PM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class ActionLog extends Base implements \Smorken\SSCommon\Contracts\Models\ActionLog
{

    protected $fillable = ['user_id', 'controller', 'action', 'ip', 'kiosk'];

    protected $friendly_columns = [
        'id' => 'ID',
        'user_id' => 'User ID',
        'controller' => 'Controller',
        'action' => 'Action',
        'ip' => 'IP',
        'kiosk' => 'Kiosk Mode',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $table = 'self_service_log';

    public function name()
    {
        return sprintf('%d', $this->id);
    }
}
