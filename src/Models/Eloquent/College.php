<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:47 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class College extends Base implements \Smorken\SSCommon\Contracts\Models\College
{

    public $incrementing = false;

    protected $fillable = ['id', 'descr', 'abbrev'];

    protected $friendly_columns = [
        'id' => 'ID',
        'descr' => 'Description',
        'abbrev' => 'Abbreviation',
    ];

    protected $rules = [
        'id' => 'required',
        'descr' => 'required',
    ];

    public function getDescriptionAttribute()
    {
        return $this->descr;
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('descr');
    }
}
