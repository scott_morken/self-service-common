<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:04 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

use Smorken\Model\Eloquent;
use Smorken\Model\Traits\FriendlyColumns;

abstract class Base extends Eloquent
{

    use FriendlyColumns;
}
