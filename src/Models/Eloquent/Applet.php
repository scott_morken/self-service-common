<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:27 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class Applet extends Base implements \Smorken\SSCommon\Contracts\Models\Applet
{

    protected $attributes = [
        'base' => '',
        'type' => 'CORE',
        'order' => 0,
    ];

    protected $fillable = [
        'code',
        'name',
        'controller',
        'handler',
        'provider',
        'applet_view',
        'base',
        'type',
        'order',
        'active',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'code' => 'Code',
        'name' => 'Name',
        'controller' => 'Controller',
        'handler' => 'Handler',
        'provider' => 'Provider',
        'applet_view' => 'Applet View',
        'base' => 'Base Path',
        'type' => 'Type',
        'order' => 'Order',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'code' => 'required|max:32',
        'name' => 'required|max:128',
        'controller' => 'required|max:128',
        'handler' => 'required|max:128',
        'provider' => 'required|max:128',
        'applet_view' => 'required|max:128',
        'base' => 'max:32',
        'type' => 'required|in:CORE,KIOSK',
        'order' => 'integer|max:999',
        'active' => 'required|boolean',
    ];

    public function getNameAttribute()
    {
        return ($this->attributes['name'] ?? null);
    }

    public function name()
    {
        return sprintf('[%s] %s', $this->code, $this->name);
    }

    public function routes()
    {
        return $this->hasMany(\Smorken\SSCommon\Models\Eloquent\Route::class, 'parent_id')
                    ->where('parent', '=', 'APP');
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('type')
                     ->orderBy('active', 'desc')
                     ->orderBy('order');
    }
}
