<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:12 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class Location extends Base implements \Smorken\SSCommon\Contracts\Models\Location
{

    protected $fillable = [
        'org_id',
        'ext_location_id',
        'descr',
        'latitude',
        'longitude',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'org_id' => 'Org ID',
        'ext_location_id' => 'External ID',
        'descr' => 'Description',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'org_id' => 'required|integer|min:1',
        'ext_location_id' => 'required',
        'descr' => 'required',
        'latitude' => 'nullable|numeric|required_with:longitude',
        'longitude' => 'nullable|numeric|required_with:latitude',
    ];

    public function getLatitudeAttribute()
    {
        $l = (float) ($this->attributes['latitude'] ?? null);
        return $l ?: null;
    }

    public function getLongitudeAttribute()
    {
        $l = (float) ($this->attributes['longitude'] ?? null);
        return $l ?: null;
    }

    public function name()
    {
        return sprintf("%s [%s]", $this->descr, $this->ext_location_id);
    }

    public function org()
    {
        return $this->belongsTo(Org::class);
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('org_id')
                     ->orderBy('descr');
    }

    public function scopeDefaultWiths($query)
    {
        return $query->with('org');
    }

    public function scopeDescrLike($query, $value)
    {
        if ($value) {
            $query->where('descr', 'LIKE', "%$value%");
        }
        return $query;
    }

    public function scopeOrgIdIs($query, $value)
    {
        if ($value) {
            $query->where('org_id', $value);
        }
        return $query;
    }
}
