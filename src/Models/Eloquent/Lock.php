<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 7:46 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

use Carbon\Carbon;

class Lock extends Base implements \Smorken\SSCommon\Contracts\Models\Lock
{

    protected $fillable = ['user_id', 'attempts'];

    /**
     * @return int
     */
    public function incrementFailures()
    {
        $this->attempts = ($this->attempts ? $this->attempts + 1 : 1);
        return $this->save();
    }

    /**
     * @param $max_count
     * @param $max_time
     * @return bool
     */
    public function isLocked($max_count, $max_time)
    {
        $locked = false;
        if ($this->user_id) {
            $now = Carbon::now();
            $diff = $now->diffInSeconds($this->updated_at);
            $locked = ($this->attempts > $max_count && $diff < $max_time);
            if ($diff > $max_time) {
                $this->delete();
            }
        }
        return $locked;
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('updated_at', 'desc');
    }

    public function scopeUserIdIs($query, $id)
    {
        return $query->where('user_id', '=', $id);
    }
}
