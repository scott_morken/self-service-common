<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:22 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class Adminer extends Base implements \Smorken\SSCommon\Contracts\Models\Adminer
{

    public $incrementing = false;

    protected $fillable = ['id', 'first_name', 'last_name'];

    protected $friendly_columns = [
        'id' => 'ID',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'id' => 'required|integer',
        'first_name' => 'required|max:32',
        'last_name' => 'required|max:32',
    ];

    public function name()
    {
        return sprintf('%s %s [%d]', $this->first_name, $this->last_name, $this->id);
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('last_name')
                     ->orderBy('first_name');
    }
}
