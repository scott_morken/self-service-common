<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:51 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class ModuleOutput extends Base implements \Smorken\SSCommon\Contracts\Models\ModuleOutput
{

    protected $attributes = [
        'view' => '',
        'icon' => '',
    ];

    protected $casts = [
        'params' => 'array',
        'options' => 'array',
    ];

    protected $fillable = [
        'module_id',
        'code',
        'name',
        'view',
        'action',
        'icon',
        'display_both',
        'show_nav',
        'params',
        'options',
        'active',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'module_id' => 'Module ID',
        'code' => 'Code',
        'name' => 'Name',
        'view' => 'View',
        'action' => 'Action',
        'icon' => 'Icon',
        'display_both' => 'Display Both',
        'show_nav' => 'Show Nav',
        'params' => 'Params',
        'options' => 'Options',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'module_id' => 'required|integer',
        'code' => 'required|max:32',
        'name' => 'required|max:128',
        'view' => 'max:128',
        'action' => 'required|max:128',
        'icon' => 'max:32',
        'display_both' => 'required|boolean',
        'show_nav' => 'required|boolean',
        'params' => 'max:255',
        'options' => 'max:255',
        'active' => 'required|boolean',
    ];

    public function action()
    {
        return explode('@', $this->getAttribute('action'));
    }

    public function getNameAttribute()
    {
        return ($this->attributes['name'] ?? null);
    }

    public function getOption($key, $default = null)
    {
        return $this->getFrom('options', $key, $default);
    }

    public function getParam($key, $default = null)
    {
        return $this->getFrom('params', $key, $default);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function name()
    {
        return sprintf('[%s] %s', $this->code, $this->name);
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('module_id')
                     ->orderBy('active', 'desc')
                     ->orderBy('code');
    }

    public function scopeDefaultWiths($query)
    {
        return $query->with('module');
    }

    public function scopeModuleIdIs($query, $id)
    {
        return $query->where('module_id', '=', $id);
    }

    protected function getFrom($what, $key, $default)
    {
        $attr = $this->getAttribute($what);
        if (!$attr) {
            $attr = [];
        }
        $v = ($attr[$key] ?? null);
        return $v !== null ? $v : $default;
    }
}
