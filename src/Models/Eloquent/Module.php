<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:27 AM
 */

namespace Smorken\SSCommon\Models\Eloquent;

class Module extends Base implements \Smorken\SSCommon\Contracts\Models\Module
{

    protected $attributes = [
        'icon' => '',
        'type' => 'CORE',
        'order' => 0,
    ];

    protected $fillable = [
        'code',
        'name',
        'controller',
        'action',
        'base',
        'icon',
        'display_both',
        'is_image',
        'type',
        'order',
        'active',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'code' => 'Code',
        'name' => 'Name',
        'controller' => 'Controller',
        'action' => 'Action',
        'base' => 'Base Path',
        'icon' => 'Icon',
        'display_both' => 'Display Both',
        'is_image' => 'Is Image',
        'type' => 'Type',
        'order' => 'Order',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    protected $rules = [
        'code' => 'required|max:32',
        'name' => 'required|max:128',
        'controller' => 'required|max:128',
        'action' => 'required|max:128',
        'base' => 'required|max:32',
        'icon' => 'max:32',
        'display_both' => 'boolean',
        'is_image' => 'boolean',
        'type' => 'required|in:CORE,KIOSK',
        'order' => 'integer|max:999',
        'active' => 'boolean',
    ];

    public function getNameAttribute()
    {
        return ($this->attributes['name'] ?? null);
    }

    public function name()
    {
        return sprintf('[%s] %s', $this->code, $this->name);
    }

    public function outputs()
    {
        return $this->hasMany(\Smorken\SSCommon\Models\Eloquent\ModuleOutput::class);
    }

    public function routes()
    {
        return $this->hasMany(\Smorken\SSCommon\Models\Eloquent\Route::class, 'parent_id')
                    ->where('parent', '=', 'MOD');
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('type')
                     ->orderBy('active', 'desc')
                     ->orderBy('order');
    }
}
