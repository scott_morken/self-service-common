<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:46 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface College
{

    /**
     * @param $id
     * @return \Smorken\SSCommon\Contracts\Models\College
     */
    public function getById($id);
}
