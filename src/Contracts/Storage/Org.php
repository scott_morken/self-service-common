<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:11 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface Org
{
    /**
     * [
     *  'ext_org_id' => 'id',
     *  ...
     * ]
     * @return array
     */
    public function getExtIdToIdMap();
}
