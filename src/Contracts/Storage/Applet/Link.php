<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/17
 * Time: 12:26 PM
 */

namespace Smorken\SSCommon\Contracts\Storage\Applet;

use Illuminate\Support\Collection;

interface Link
{

    /**
     * Return active links
     * @return Collection<\Smorken\Sscommon\Contracts\Models\Applet\Link>
     */
    public function load();
}
