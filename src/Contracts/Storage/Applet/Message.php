<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Storage\Applet;

interface Message
{

    /**
     * @param bool $login_page
     * @return \Smorken\SSCommon\Contracts\Models\Applet\Message[]
     */
    public function activeMessages($login_page = false);
}
