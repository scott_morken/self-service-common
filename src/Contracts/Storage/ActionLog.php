<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/7/14
 * Time: 4:20 PM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface ActionLog
{

    public function store(array $data);
}
