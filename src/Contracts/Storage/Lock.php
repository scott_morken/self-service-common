<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 7:36 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface Lock
{

    /**
     * @param $user_id
     * @param $max_count
     * @param $max_time
     * @return bool
     */
    public function isLocked($user_id, $max_count, $max_time);

    /**
     * @param $user_id
     * @return int
     */
    public function incrementFailures($user_id);

    /**
     * @param $user_id
     * @return int
     */
    public function clear($user_id);
}
