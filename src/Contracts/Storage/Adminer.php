<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface Adminer
{

    public function firstOrCreateById($id, array $attributes);

    /**
     * @param $id
     * @return bool
     */
    public function isAllowed($id);
}
