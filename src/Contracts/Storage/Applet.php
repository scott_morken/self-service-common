<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

use Illuminate\Routing\Router;

interface Applet
{

    public function active();

    public function getApplet($name);

    public function getApplets($type = \Smorken\SSCommon\Contracts\Models\Applet::CORE);

    /**
     * @return Router
     */
    public function getRouter();

    public function getRoutesFrom($model, $type, $prefix = null);

    public function getRunnable($type = \Smorken\SSCommon\Contracts\Models\Applet::CORE);

    /**
     * @param  Router  $route
     * @return void
     */
    public function setRouter(Router $route);
}
