<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:07 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

interface Module
{

    public function active();

    public function getModule($name);

    public function getModules($type = \Smorken\SSCommon\Contracts\Models\Module::CORE);

    public function getRoutesFrom($model, $type, $prefix = null);

    public function getRunnable($type = \Smorken\SSCommon\Contracts\Models\Module::CORE);
}
