<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:10 AM
 */

namespace Smorken\SSCommon\Contracts\Storage;

/**
 * Interface Location
 * @package Smorken\SSCommon\Contracts\Storage
 */
interface Location
{

    public function import(array $data);

    public function getByOrgId($org_id);
}
