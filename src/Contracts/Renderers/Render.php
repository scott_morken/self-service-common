<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/24/16
 * Time: 11:12 AM
 */

namespace Smorken\SSCommon\Contracts\Renderers;

interface Render
{

    public function render($data);
}
