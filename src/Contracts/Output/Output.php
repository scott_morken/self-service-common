<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/13/17
 * Time: 8:33 AM
 */

namespace Smorken\SSCommon\Contracts\Output;

/**
 * Interface Output
 * @package Smorken\SSCommon\Contracts\Output
 *
 * Contract used by 'outputs'
 * email, print, applet, etc
 */
interface Output
{

    public function run();

    public function inject($data);

    public function getInjected($key);

    public function setDefaults($options);

    public function getDefaults();

    public function getDefault($key);

    public function getOption($key);

    public function getAllOptions();
}
