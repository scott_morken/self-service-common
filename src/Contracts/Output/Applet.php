<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/13/17
 * Time: 8:34 AM
 */

namespace Smorken\SSCommon\Contracts\Output;

interface Applet
{

    /**
     * @return \Smorken\SSCommon\Contracts\Models\Applet
     */
    public function getSourceModel();

    /**
     * @param \Smorken\SSCommon\Contracts\Models\Applet $applet
     * @return void
     */
    public function setSourceModel(\Smorken\SSCommon\Contracts\Models\Applet $applet);

    /**
     * @return mixed
     */
    public function getProvider();

    /**
     * @param $provider
     * @return void
     */
    public function setProvider($provider);
}
