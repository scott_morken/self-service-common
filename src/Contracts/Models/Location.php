<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:09 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Location
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property int $org_id
 * @property string $ext_location_id
 * @property string $descr
 * @property float|null $latitude
 * @property float|null $longitude
 *
 * @property Org $org
 */
interface Location
{

}
