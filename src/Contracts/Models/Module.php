<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:07 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Module
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $controller
 * @property string $action
 * @property string $base
 * @property string $icon
 * @property bool $display_both
 * @property bool $is_image
 * @property string $type
 * @property int $order
 * @property bool $active
 *
 * @property Route[] $routes
 * @property ModuleOutput[] $outputs
 */
interface Module
{

    const KIOSK = 'KIOSK';
    const CORE = 'CORE';

    public function routes();

    public function outputs();
}
