<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/30/16
 * Time: 7:33 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Lock
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property int $user_id
 * @property int $attempts
 */
interface Lock
{

    /**
     * @param $max_count
     * @param $max_time
     * @return bool
     */
    public function isLocked($max_count, $max_time);

    /**
     * @return int
     */
    public function incrementFailures();
}
