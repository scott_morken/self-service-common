<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:07 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Class Route
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property string $parent
 * @property int $parent_id
 * @property string $action
 * @property string $path
 */
interface Route
{

}
