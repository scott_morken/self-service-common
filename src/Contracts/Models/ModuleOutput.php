<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:07 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface ModuleOutput
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property int $module_id
 * @property string $code
 * @property string $name
 * @property string $view
 * @property string $action
 * @proeprty string $icon
 * @property bool $display_both
 * @property bool $show_nav
 * @property mixed $params
 * @property mixed $options
 * @property bool $active
 *
 * @property Module $module
 */
interface ModuleOutput
{

    public function getOption($key, $default = null);

    public function getParam($key, $default = null);

    public function action();
}
