<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/25/17
 * Time: 12:25 PM
 */

namespace Smorken\SSCommon\Contracts\Models\Applet;

/**
 * Interface Link
 * @package Smorken\SSCommon\Contracts\Models\Applet
 *
 * @property int $id
 * @property string $title
 * @property string $href
 * @property string $target
 * @property string $classes
 * @property bool $active
 * @property int $weight
 */
interface Link
{

}
