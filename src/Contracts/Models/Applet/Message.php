<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Models\Applet;

use Carbon\Carbon;

/**
 * Interface Message
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property string $message
 * @property string $description
 * @property bool $active
 * @property Carbon|null $start_date
 * @property Carbon|null $end_date
 * @property int $order
 * @property bool $login_page
 * @property string $classes
 * @property string $container
 */
interface Message
{

    public function asSelect($items);

    public function getAllowedContainers();

    public function getAllowedClasses();
}
