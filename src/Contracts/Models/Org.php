<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:09 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Org
 * @package Smorken\SSCommon\Contracts\Models
 * @property int $id
 * @property string $ext_org_id
 * @property string $descr
 * @property float|null $latitude
 * @property float|null $longitude
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Location[] $locations
 */
interface Org
{

}
