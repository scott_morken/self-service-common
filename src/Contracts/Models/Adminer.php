<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Adminer
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 */
interface Adminer
{

}
