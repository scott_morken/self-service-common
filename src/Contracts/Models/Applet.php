<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/18/16
 * Time: 9:06 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface Applet
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $controller
 * @property string $handler
 * @property string $provider
 * @property string $applet_view
 * @property string $base
 * @property string $type
 * @property int $order
 * @property bool $active
 *
 * @property Route[] $routes
 */
interface Applet
{
    const KIOSK = 'KIOSK';
    const CORE = 'CORE';

    public function routes();
}
