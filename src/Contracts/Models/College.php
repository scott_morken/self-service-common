<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:46 AM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface College
 * @package Smorken\SSCommon\Contracts\Models
 *
 * @property string $id
 * @property string $descr
 * @property string $description
 * @property string $abbrev
 */
interface College
{

}
