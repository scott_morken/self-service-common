<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/31/15
 * Time: 3:02 PM
 */

namespace Smorken\SSCommon\Contracts\Models;

/**
 * Interface ActionLog
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property string $user_id
 * @property string $controller
 * @property string $action
 * @property string $ip
 * @property bool $kiosk
 */
interface ActionLog
{

}
