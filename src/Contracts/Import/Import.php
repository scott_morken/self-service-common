<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/22/17
 * Time: 9:10 AM
 */

namespace Smorken\SSCommon\Contracts\Import;

interface Import
{
    /**
     * @param $filename
     * @return int    number of rows imported
     */
    public function import($filename);

    /**
     * @return bool
     */
    public function hasErrors();

    /**
     * @return array
     */
    public function getErrors();
}
