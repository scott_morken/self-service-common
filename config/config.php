<?php
return [
    'messages' => [
        'containers' => [
            'div',
            'p',
            'small',
            'span',
        ],
        'classes' => [
            'alert alert-danger',
            'alert alert-warning',
            'alert alert-success',
            'alert alert-info',
        ]
    ],
];
